setuppath='/afs/cern.ch/user/a/akourkou/work/CSC_noDep'
echo "doing file = : athena runJobs.py"


#function Batch()
#{
bsub <<CmdBlock
#BSUB -q 8nh
#BSUB -J job_CSC
#BSUB -o report
#BSUB -n 4
#BSUB -C 1024
#BSUB -R "type==SLC6_64&&pool>1024"
cd $setuppath/CSCntuple/cmt
cmt config
make
cd $setuppath/CSCntuple/run
source $setuppath/setup_batch.sh
echo "command: athena runJobs.py"
athena runJobs.py
echo "Job done!"
CmdBlock
#}
