export PATHENA_GRID_SETUP_SH=/dev/null
source /afs/cern.ch/atlas/offline/external/GRID/DA/panda-client/latest/etc/panda/panda_setup.sh

inFile1=data15_13TeV.00267638.physics_Main.recon.ESD.f603 #Toroid ON, L=3.7 pb-1
inFile2=data15_13TeV.00267639.physics_Main.recon.ESD.f603 #Toroid ON, L=3.4 pb-1
inFile3=data15_13TeV.00271516.physics_Main.recon.ESD.f611 #Toroid ON, L=21  pb-1
inFile4=data15_13TeV.00271595.physics_Main.recon.ESD.f611 #Toroid ON, L=19.9  pb-1
inFile5=data15_cos.00275880.express_express.recon.ESD.f617 #cosmic express stream for michael
inFile6=data15_13TeV.00279284.physics_Main.recon.ESD.f628 #Toroid ON, low HV
inFile7=data15_13TeV.00278880.physics_Main.recon.ESD.f628 #Toroid ON, -100 V
inFile8=data15_13TeV.00278748.physics_Main.recon.ESD.f628 #Toroid ON, -0 V

#pathena runJobs.py --inDS ${inFile1}  --outDS user.akourkou.00267638.physics_Main.recon.ESD.f603_06 --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

#pathena runJobs.py --inDS ${inFile2}  --outDS user.akourkou.00267639.physics_Main.recon.ESD.f603_03 --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

#pathena runJobs.py --inDS ${inFile3}  --outDS user.akourkou.00271516.physics_Main.recon.ESD.f611_noClus --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

#pathena runJobs.py --inDS ${inFile4}  --outDS user.akourkou.00271595.physics_Main.recon.ESD.f611_noClus --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

#pathena runJobs.py --inDS ${inFile5}  --outDS user.akourkou.00275880.express_express.recon.ESD.f617_wClus2 --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

pathena runJobs.py --inDS ${inFile6}  --outDS user.akourkou.00279284.physics_Main.recon.ESD.f628_noClus --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

pathena runJobs.py --inDS ${inFile7}  --outDS user.akourkou.00278880.physics_Main.recon.ESD.f628_noClus --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

pathena runJobs.py --inDS ${inFile8}  --outDS user.akourkou.00278748.physics_Main.recon.ESD.f628_noClus --nFilesPerJob 3 --supStream GLOBAL --extOutFile 13TeV_Muons.ExtCSCValid.root

