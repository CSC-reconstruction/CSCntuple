import sys,os
from AthenaCommon.Include import include#Athina


#os.system('source /afs/cern.ch/atlas/offline/external/GRID/DA/panda-client/latest/etc/panda/panda_setup.sh')
#pathena -c "ReadMC=True" runJobs.py --inDS  --outDS user.akourkou. --nFilesPerJob 3 --supStream GLOBAL --extOutFile ntuple.root
#pathena runJobs.py --inDS  --outDS user.akourkou. --nFilesPerJob 3 --supStream GLOBAL --extOutFile ntuple.root




#def include(filename):
#    if os.path.exists(filename): 
#        execfile(filename)
#    else:
#        print 'file/path does not exist'

myname = "runJobs.py: "
mygenfiles = [
    #"../../data15_cos.00252223.physics_IDCosmic.recon.ESD.f531/data15_cos.00252223.physics_IDCosmic.recon.ESD.f531._lb0059._SFO-ALL._0001.1",
    #"root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data15_13TeV/physics_Main/00267638/data15_13TeV.00267638.physics_Main.recon.ESD.f603/data15_13TeV.00267638.physics_Main.recon.ESD.f603._lb0419._SFO-6._0002._001.1"
    #"root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data15_cos/express_express/00275880/data15_cos.00275880.express_express.recon.ESD.f617/data15_cos.00275880.express_express.recon.ESD.f617._lb0129._SFO-ALL._0001_003.1"
    "root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data15_13TeV/physics_Main/00279284/data15_13TeV.00279284.physics_Main.recon.ESD.f628/data15_13TeV.00279284.physics_Main.recon.ESD.f628._lb0882._SFO-6._0002_003.1"
    ]
InputFiles = mygenfiles
ReadMC = True # default is false
outbase = "ExtCSCValid"
ListFile = '13TeV_Muons'
NtupleFileName = ListFile + "." + outbase + ".root"
#NumberOfEvents = 100 #poppy
#SkipEvents = 1000
Verbose =False

include("../share/SimpleJobOption2.py")#RunCSCntuple.py")
#include("../share/RunCSCntuple.py")

