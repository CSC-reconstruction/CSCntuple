gROOT->Reset();

#include "TMatrix.h"
#include "TSQLResult.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TChain.h"
#include "TTree.h"

#include <iostream>
#include <fstream>
#include <string>
  
using namespace std;

bool RunMuonTrackInfo(){  
  gSystem->CompileMacro("MuonTrackInfo.C","gk");
  TChain* chain = new TChain("chain","chain");

 /* if (fileType=="slep"){

  	if (filename=="110_90") chain->Add("Nominal_176531.root/id_176531");
  	else if (filename=="135_115") chain->Add("Nominal_176532.root/id_176532");
	else if (filename=="185_165") chain->Add("Nominal_176533.root/id_176533");

	else if (filename=="112_47") chain->Add("Nominal_176536.root/id_176536");
	else if (filename=="132_67") chain->Add("Nominal_176537.root/id_176537");
	else if (filename=="157_92") chain->Add("Nominal_176538.root/id_176538");
  } 
  else if (fileType=="noslep"){

  	if (filename=="100_75") chain->Add("Nominal_174678.root/id_174678");
  	else if (filename=="125_100") chain->Add("Nominal_174839.root/id_174839");
	else if (filename=="100_87") chain->Add("Nominal_178792.root/id_178792");

  }*/

  MuonTrackInfo t(chain);
  t.Loop();
  return 0;
}


