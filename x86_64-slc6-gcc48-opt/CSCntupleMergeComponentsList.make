#-- start of make_header -----------------

#====================================
#  Document CSCntupleMergeComponentsList
#
#   Generated Mon Sep 21 21:49:32 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleMergeComponentsList

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleMergeComponentsList = $(CSCntuple_tag)_CSCntupleMergeComponentsList.make
cmt_local_tagfile_CSCntupleMergeComponentsList = $(bin)$(CSCntuple_tag)_CSCntupleMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleMergeComponentsList = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleMergeComponentsList = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleMergeComponentsList)
#-include $(cmt_local_tagfile_CSCntupleMergeComponentsList)

ifdef cmt_CSCntupleMergeComponentsList_has_target_tag

cmt_final_setup_CSCntupleMergeComponentsList = $(bin)setup_CSCntupleMergeComponentsList.make
cmt_dependencies_in_CSCntupleMergeComponentsList = $(bin)dependencies_CSCntupleMergeComponentsList.in
#cmt_final_setup_CSCntupleMergeComponentsList = $(bin)CSCntuple_CSCntupleMergeComponentsListsetup.make
cmt_local_CSCntupleMergeComponentsList_makefile = $(bin)CSCntupleMergeComponentsList.make

else

cmt_final_setup_CSCntupleMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_CSCntupleMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_CSCntupleMergeComponentsList = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleMergeComponentsList_makefile = $(bin)CSCntupleMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleMergeComponentsList/
#CSCntupleMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: CSCntupleMergeComponentsList CSCntupleMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/CSCntuple.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

CSCntupleMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  CSCntupleMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

CSCntupleMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libCSCntuple_so_dependencies = ../x86_64-slc6-gcc48-opt/libCSCntuple.so
#-- start of cleanup_header --------------

clean :: CSCntupleMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleMergeComponentsListclean ::
#-- end of cleanup_header ---------------
