#-- start of make_header -----------------

#====================================
#  Document CSCntupleComponentsList
#
#   Generated Mon Sep 21 21:49:30 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleComponentsList

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleComponentsList = $(CSCntuple_tag)_CSCntupleComponentsList.make
cmt_local_tagfile_CSCntupleComponentsList = $(bin)$(CSCntuple_tag)_CSCntupleComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleComponentsList = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleComponentsList = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleComponentsList)
#-include $(cmt_local_tagfile_CSCntupleComponentsList)

ifdef cmt_CSCntupleComponentsList_has_target_tag

cmt_final_setup_CSCntupleComponentsList = $(bin)setup_CSCntupleComponentsList.make
cmt_dependencies_in_CSCntupleComponentsList = $(bin)dependencies_CSCntupleComponentsList.in
#cmt_final_setup_CSCntupleComponentsList = $(bin)CSCntuple_CSCntupleComponentsListsetup.make
cmt_local_CSCntupleComponentsList_makefile = $(bin)CSCntupleComponentsList.make

else

cmt_final_setup_CSCntupleComponentsList = $(bin)setup.make
cmt_dependencies_in_CSCntupleComponentsList = $(bin)dependencies.in
#cmt_final_setup_CSCntupleComponentsList = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleComponentsList_makefile = $(bin)CSCntupleComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleComponentsList/
#CSCntupleComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = CSCntuple.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libCSCntuple.$(shlibsuffix)

CSCntupleComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: CSCntupleComponentsListinstall
CSCntupleComponentsListinstall :: CSCntupleComponentsList

uninstall :: CSCntupleComponentsListuninstall
CSCntupleComponentsListuninstall :: CSCntupleComponentsListclean

CSCntupleComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: CSCntupleComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleComponentsListclean ::
#-- end of cleanup_header ---------------
