#-- start of make_header -----------------

#====================================
#  Document CSCntupleConfDbMerge
#
#   Generated Mon Sep 21 21:49:30 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleConfDbMerge

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleConfDbMerge = $(CSCntuple_tag)_CSCntupleConfDbMerge.make
cmt_local_tagfile_CSCntupleConfDbMerge = $(bin)$(CSCntuple_tag)_CSCntupleConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleConfDbMerge = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleConfDbMerge = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleConfDbMerge)
#-include $(cmt_local_tagfile_CSCntupleConfDbMerge)

ifdef cmt_CSCntupleConfDbMerge_has_target_tag

cmt_final_setup_CSCntupleConfDbMerge = $(bin)setup_CSCntupleConfDbMerge.make
cmt_dependencies_in_CSCntupleConfDbMerge = $(bin)dependencies_CSCntupleConfDbMerge.in
#cmt_final_setup_CSCntupleConfDbMerge = $(bin)CSCntuple_CSCntupleConfDbMergesetup.make
cmt_local_CSCntupleConfDbMerge_makefile = $(bin)CSCntupleConfDbMerge.make

else

cmt_final_setup_CSCntupleConfDbMerge = $(bin)setup.make
cmt_dependencies_in_CSCntupleConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_CSCntupleConfDbMerge = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleConfDbMerge_makefile = $(bin)CSCntupleConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleConfDbMerge/
#CSCntupleConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: CSCntupleConfDbMerge CSCntupleConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple/CSCntuple.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

CSCntupleConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  CSCntupleConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

CSCntupleConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libCSCntuple_so_dependencies = ../x86_64-slc6-gcc48-opt/libCSCntuple.so
#-- start of cleanup_header --------------

clean :: CSCntupleConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleConfDbMergeclean ::
#-- end of cleanup_header ---------------
