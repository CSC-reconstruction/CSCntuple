#-- start of make_header -----------------

#====================================
#  Library CSCntupleLib
#
#   Generated Mon Sep 21 21:48:31 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleLib

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleLib = $(CSCntuple_tag)_CSCntupleLib.make
cmt_local_tagfile_CSCntupleLib = $(bin)$(CSCntuple_tag)_CSCntupleLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleLib = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleLib = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleLib)
#-include $(cmt_local_tagfile_CSCntupleLib)

ifdef cmt_CSCntupleLib_has_target_tag

cmt_final_setup_CSCntupleLib = $(bin)setup_CSCntupleLib.make
cmt_dependencies_in_CSCntupleLib = $(bin)dependencies_CSCntupleLib.in
#cmt_final_setup_CSCntupleLib = $(bin)CSCntuple_CSCntupleLibsetup.make
cmt_local_CSCntupleLib_makefile = $(bin)CSCntupleLib.make

else

cmt_final_setup_CSCntupleLib = $(bin)setup.make
cmt_dependencies_in_CSCntupleLib = $(bin)dependencies.in
#cmt_final_setup_CSCntupleLib = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleLib_makefile = $(bin)CSCntupleLib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleLib/
#CSCntupleLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

CSCntupleLiblibname   = $(bin)$(library_prefix)CSCntupleLib$(library_suffix)
CSCntupleLiblib       = $(CSCntupleLiblibname).a
CSCntupleLibstamp     = $(bin)CSCntupleLib.stamp
CSCntupleLibshstamp   = $(bin)CSCntupleLib.shstamp

CSCntupleLib :: dirs  CSCntupleLibLIB
	$(echo) "CSCntupleLib ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#CSCntupleLibLIB :: $(CSCntupleLiblib) $(CSCntupleLibshstamp)
CSCntupleLibLIB :: $(CSCntupleLibshstamp)
	$(echo) "CSCntupleLib : library ok"

$(CSCntupleLiblib) :: $(bin)CreateNtuple.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(CSCntupleLiblib) $?
	$(lib_silent) $(ranlib) $(CSCntupleLiblib)
	$(lib_silent) cat /dev/null >$(CSCntupleLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(CSCntupleLiblibname).$(shlibsuffix) :: $(bin)CreateNtuple.o $(use_requirements) $(CSCntupleLibstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)CreateNtuple.o $(CSCntupleLib_shlibflags)
	$(lib_silent) cat /dev/null >$(CSCntupleLibstamp) && \
	  cat /dev/null >$(CSCntupleLibshstamp)

$(CSCntupleLibshstamp) :: $(CSCntupleLiblibname).$(shlibsuffix)
	$(lib_silent) if test -f $(CSCntupleLiblibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(CSCntupleLibstamp) && \
	  cat /dev/null >$(CSCntupleLibshstamp) ; fi

CSCntupleLibclean ::
	$(cleanup_echo) objects CSCntupleLib
	$(cleanup_silent) /bin/rm -f $(bin)CreateNtuple.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)CreateNtuple.o) $(patsubst %.o,%.dep,$(bin)CreateNtuple.o) $(patsubst %.o,%.d.stamp,$(bin)CreateNtuple.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf CSCntupleLib_deps CSCntupleLib_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
CSCntupleLibinstallname = $(library_prefix)CSCntupleLib$(library_suffix).$(shlibsuffix)

CSCntupleLib :: CSCntupleLibinstall ;

install :: CSCntupleLibinstall ;

CSCntupleLibinstall :: $(install_dir)/$(CSCntupleLibinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(CSCntupleLibinstallname) :: $(bin)$(CSCntupleLibinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(CSCntupleLibinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##CSCntupleLibclean :: CSCntupleLibuninstall

uninstall :: CSCntupleLibuninstall ;

CSCntupleLibuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(CSCntupleLibinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),CSCntupleLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)CreateNtuple.d

$(bin)$(binobj)CreateNtuple.d :

$(bin)$(binobj)CreateNtuple.o : $(cmt_final_setup_CSCntupleLib)

$(bin)$(binobj)CreateNtuple.o : $(src)CreateNtuple.cxx
	$(cpp_echo) $(src)CreateNtuple.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(CSCntupleLib_pp_cppflags) $(lib_CSCntupleLib_pp_cppflags) $(CreateNtuple_pp_cppflags) $(use_cppflags) $(CSCntupleLib_cppflags) $(lib_CSCntupleLib_cppflags) $(CreateNtuple_cppflags) $(CreateNtuple_cxx_cppflags)  $(src)CreateNtuple.cxx
endif
endif

else
$(bin)CSCntupleLib_dependencies.make : $(CreateNtuple_cxx_dependencies)

$(bin)CSCntupleLib_dependencies.make : $(src)CreateNtuple.cxx

$(bin)$(binobj)CreateNtuple.o : $(CreateNtuple_cxx_dependencies)
	$(cpp_echo) $(src)CreateNtuple.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(CSCntupleLib_pp_cppflags) $(lib_CSCntupleLib_pp_cppflags) $(CreateNtuple_pp_cppflags) $(use_cppflags) $(CSCntupleLib_cppflags) $(lib_CSCntupleLib_cppflags) $(CreateNtuple_cppflags) $(CreateNtuple_cxx_cppflags)  $(src)CreateNtuple.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: CSCntupleLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleLib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library CSCntupleLib
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)CSCntupleLib$(library_suffix).a $(library_prefix)CSCntupleLib$(library_suffix).$(shlibsuffix) CSCntupleLib.stamp CSCntupleLib.shstamp
#-- end of cleanup_library ---------------
