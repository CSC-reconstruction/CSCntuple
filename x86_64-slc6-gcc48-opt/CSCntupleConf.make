#-- start of make_header -----------------

#====================================
#  Document CSCntupleConf
#
#   Generated Mon Sep 21 21:49:24 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleConf

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleConf = $(CSCntuple_tag)_CSCntupleConf.make
cmt_local_tagfile_CSCntupleConf = $(bin)$(CSCntuple_tag)_CSCntupleConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleConf = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleConf = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleConf)
#-include $(cmt_local_tagfile_CSCntupleConf)

ifdef cmt_CSCntupleConf_has_target_tag

cmt_final_setup_CSCntupleConf = $(bin)setup_CSCntupleConf.make
cmt_dependencies_in_CSCntupleConf = $(bin)dependencies_CSCntupleConf.in
#cmt_final_setup_CSCntupleConf = $(bin)CSCntuple_CSCntupleConfsetup.make
cmt_local_CSCntupleConf_makefile = $(bin)CSCntupleConf.make

else

cmt_final_setup_CSCntupleConf = $(bin)setup.make
cmt_dependencies_in_CSCntupleConf = $(bin)dependencies.in
#cmt_final_setup_CSCntupleConf = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleConf_makefile = $(bin)CSCntupleConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleConf/
#CSCntupleConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: CSCntupleConf CSCntupleConfclean

confpy  := CSCntupleConf.py
conflib := $(bin)$(library_prefix)CSCntuple.$(shlibsuffix)
confdb  := CSCntuple.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

CSCntupleConf :: CSCntupleConfinstall

install :: CSCntupleConfinstall

CSCntupleConfinstall : /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple/$(confpy)
	@echo "Installing /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple in /afs/cern.ch/work/a/akourkou/CSC_noDep/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple /afs/cern.ch/work/a/akourkou/CSC_noDep/InstallArea/python ; \

/afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple/$(confpy) : $(conflib) /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)CSCntuple.$(shlibsuffix)

/afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple:
	@ if [ ! -d /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple ] ; then mkdir -p /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple ; fi ;

CSCntupleConfclean :: CSCntupleConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple/$(confpy) /afs/cern.ch/user/a/akourkou/work/CSC_noDep/CSCntuple/genConf/CSCntuple/$(confdb)

uninstall :: CSCntupleConfuninstall

CSCntupleConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/work/a/akourkou/CSC_noDep/InstallArea/python
libCSCntuple_so_dependencies = ../x86_64-slc6-gcc48-opt/libCSCntuple.so
#-- start of cleanup_header --------------

clean :: CSCntupleConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleConfclean ::
#-- end of cleanup_header ---------------
