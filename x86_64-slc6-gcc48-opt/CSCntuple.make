#-- start of make_header -----------------

#====================================
#  Library CSCntuple
#
#   Generated Mon Sep 21 21:49:06 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntuple_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntuple_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntuple

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntuple = $(CSCntuple_tag)_CSCntuple.make
cmt_local_tagfile_CSCntuple = $(bin)$(CSCntuple_tag)_CSCntuple.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntuple = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntuple = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntuple)
#-include $(cmt_local_tagfile_CSCntuple)

ifdef cmt_CSCntuple_has_target_tag

cmt_final_setup_CSCntuple = $(bin)setup_CSCntuple.make
cmt_dependencies_in_CSCntuple = $(bin)dependencies_CSCntuple.in
#cmt_final_setup_CSCntuple = $(bin)CSCntuple_CSCntuplesetup.make
cmt_local_CSCntuple_makefile = $(bin)CSCntuple.make

else

cmt_final_setup_CSCntuple = $(bin)setup.make
cmt_dependencies_in_CSCntuple = $(bin)dependencies.in
#cmt_final_setup_CSCntuple = $(bin)CSCntuplesetup.make
cmt_local_CSCntuple_makefile = $(bin)CSCntuple.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntuple :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntuple'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntuple/
#CSCntuple::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

CSCntuplelibname   = $(bin)$(library_prefix)CSCntuple$(library_suffix)
CSCntuplelib       = $(CSCntuplelibname).a
CSCntuplestamp     = $(bin)CSCntuple.stamp
CSCntupleshstamp   = $(bin)CSCntuple.shstamp

CSCntuple :: dirs  CSCntupleLIB
	$(echo) "CSCntuple ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#CSCntupleLIB :: $(CSCntuplelib) $(CSCntupleshstamp)
CSCntupleLIB :: $(CSCntupleshstamp)
	$(echo) "CSCntuple : library ok"

$(CSCntuplelib) :: $(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(CSCntuplelib) $?
	$(lib_silent) $(ranlib) $(CSCntuplelib)
	$(lib_silent) cat /dev/null >$(CSCntuplestamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(CSCntuplelibname).$(shlibsuffix) :: $(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o $(use_requirements) $(CSCntuplestamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o $(CSCntuple_shlibflags)
	$(lib_silent) cat /dev/null >$(CSCntuplestamp) && \
	  cat /dev/null >$(CSCntupleshstamp)

$(CSCntupleshstamp) :: $(CSCntuplelibname).$(shlibsuffix)
	$(lib_silent) if test -f $(CSCntuplelibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(CSCntuplestamp) && \
	  cat /dev/null >$(CSCntupleshstamp) ; fi

CSCntupleclean ::
	$(cleanup_echo) objects CSCntuple
	$(cleanup_silent) /bin/rm -f $(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o) $(patsubst %.o,%.dep,$(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o) $(patsubst %.o,%.d.stamp,$(bin)CSCntuple_load.o $(bin)CSCntuple_entries.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf CSCntuple_deps CSCntuple_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
CSCntupleinstallname = $(library_prefix)CSCntuple$(library_suffix).$(shlibsuffix)

CSCntuple :: CSCntupleinstall ;

install :: CSCntupleinstall ;

CSCntupleinstall :: $(install_dir)/$(CSCntupleinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(CSCntupleinstallname) :: $(bin)$(CSCntupleinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(CSCntupleinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##CSCntupleclean :: CSCntupleuninstall

uninstall :: CSCntupleuninstall ;

CSCntupleuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(CSCntupleinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),CSCntupleclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)CSCntuple_load.d

$(bin)$(binobj)CSCntuple_load.d :

$(bin)$(binobj)CSCntuple_load.o : $(cmt_final_setup_CSCntuple)

$(bin)$(binobj)CSCntuple_load.o : $(src)components/CSCntuple_load.cxx
	$(cpp_echo) $(src)components/CSCntuple_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(CSCntuple_pp_cppflags) $(lib_CSCntuple_pp_cppflags) $(CSCntuple_load_pp_cppflags) $(use_cppflags) $(CSCntuple_cppflags) $(lib_CSCntuple_cppflags) $(CSCntuple_load_cppflags) $(CSCntuple_load_cxx_cppflags) -I../src/components $(src)components/CSCntuple_load.cxx
endif
endif

else
$(bin)CSCntuple_dependencies.make : $(CSCntuple_load_cxx_dependencies)

$(bin)CSCntuple_dependencies.make : $(src)components/CSCntuple_load.cxx

$(bin)$(binobj)CSCntuple_load.o : $(CSCntuple_load_cxx_dependencies)
	$(cpp_echo) $(src)components/CSCntuple_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(CSCntuple_pp_cppflags) $(lib_CSCntuple_pp_cppflags) $(CSCntuple_load_pp_cppflags) $(use_cppflags) $(CSCntuple_cppflags) $(lib_CSCntuple_cppflags) $(CSCntuple_load_cppflags) $(CSCntuple_load_cxx_cppflags) -I../src/components $(src)components/CSCntuple_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),CSCntupleclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)CSCntuple_entries.d

$(bin)$(binobj)CSCntuple_entries.d :

$(bin)$(binobj)CSCntuple_entries.o : $(cmt_final_setup_CSCntuple)

$(bin)$(binobj)CSCntuple_entries.o : $(src)components/CSCntuple_entries.cxx
	$(cpp_echo) $(src)components/CSCntuple_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(CSCntuple_pp_cppflags) $(lib_CSCntuple_pp_cppflags) $(CSCntuple_entries_pp_cppflags) $(use_cppflags) $(CSCntuple_cppflags) $(lib_CSCntuple_cppflags) $(CSCntuple_entries_cppflags) $(CSCntuple_entries_cxx_cppflags) -I../src/components $(src)components/CSCntuple_entries.cxx
endif
endif

else
$(bin)CSCntuple_dependencies.make : $(CSCntuple_entries_cxx_dependencies)

$(bin)CSCntuple_dependencies.make : $(src)components/CSCntuple_entries.cxx

$(bin)$(binobj)CSCntuple_entries.o : $(CSCntuple_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/CSCntuple_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(CSCntuple_pp_cppflags) $(lib_CSCntuple_pp_cppflags) $(CSCntuple_entries_pp_cppflags) $(use_cppflags) $(CSCntuple_cppflags) $(lib_CSCntuple_cppflags) $(CSCntuple_entries_cppflags) $(CSCntuple_entries_cxx_cppflags) -I../src/components $(src)components/CSCntuple_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: CSCntupleclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntuple.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library CSCntuple
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)CSCntuple$(library_suffix).a $(library_prefix)CSCntuple$(library_suffix).$(shlibsuffix) CSCntuple.stamp CSCntuple.shstamp
#-- end of cleanup_library ---------------
