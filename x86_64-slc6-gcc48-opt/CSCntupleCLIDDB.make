#-- start of make_header -----------------

#====================================
#  Document CSCntupleCLIDDB
#
#   Generated Mon Sep 21 21:49:24 2015  by akourkou
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_CSCntupleCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_CSCntupleCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_CSCntupleCLIDDB

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleCLIDDB = $(CSCntuple_tag)_CSCntupleCLIDDB.make
cmt_local_tagfile_CSCntupleCLIDDB = $(bin)$(CSCntuple_tag)_CSCntupleCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

CSCntuple_tag = $(tag)

#cmt_local_tagfile_CSCntupleCLIDDB = $(CSCntuple_tag).make
cmt_local_tagfile_CSCntupleCLIDDB = $(bin)$(CSCntuple_tag).make

endif

include $(cmt_local_tagfile_CSCntupleCLIDDB)
#-include $(cmt_local_tagfile_CSCntupleCLIDDB)

ifdef cmt_CSCntupleCLIDDB_has_target_tag

cmt_final_setup_CSCntupleCLIDDB = $(bin)setup_CSCntupleCLIDDB.make
cmt_dependencies_in_CSCntupleCLIDDB = $(bin)dependencies_CSCntupleCLIDDB.in
#cmt_final_setup_CSCntupleCLIDDB = $(bin)CSCntuple_CSCntupleCLIDDBsetup.make
cmt_local_CSCntupleCLIDDB_makefile = $(bin)CSCntupleCLIDDB.make

else

cmt_final_setup_CSCntupleCLIDDB = $(bin)setup.make
cmt_dependencies_in_CSCntupleCLIDDB = $(bin)dependencies.in
#cmt_final_setup_CSCntupleCLIDDB = $(bin)CSCntuplesetup.make
cmt_local_CSCntupleCLIDDB_makefile = $(bin)CSCntupleCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)CSCntuplesetup.make

#CSCntupleCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'CSCntupleCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = CSCntupleCLIDDB/
#CSCntupleCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: CSCntupleCLIDDB CSCntupleCLIDDBclean

outname := clid.db
cliddb  := CSCntuple_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)CSCntuple.$(shlibsuffix)

CSCntupleCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p CSCntuple -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

CSCntupleCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: CSCntupleCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(CSCntupleCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

CSCntupleCLIDDBclean ::
#-- end of cleanup_header ---------------
