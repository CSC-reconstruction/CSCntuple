# echo "cleanup CSCntuple CSCntuple-r0 in /afs/cern.ch/user/a/akourkou/work/CSC_noDep"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.6.0/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtCSCntupletempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtCSCntupletempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=CSCntuple -version=CSCntuple-r0 -path=/afs/cern.ch/user/a/akourkou/work/CSC_noDep  $* >${cmtCSCntupletempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=CSCntuple -version=CSCntuple-r0 -path=/afs/cern.ch/user/a/akourkou/work/CSC_noDep  $* >${cmtCSCntupletempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtCSCntupletempfile}
  unset cmtCSCntupletempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtCSCntupletempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtCSCntupletempfile}
unset cmtCSCntupletempfile
return $cmtcleanupstatus

