# echo "setup CSCntuple CSCntuple-r0 in /afs/cern.ch/user/a/akourkou/work/CSC_noDep"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.6.0/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtCSCntupletempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtCSCntupletempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=CSCntuple -version=CSCntuple-r0 -path=/afs/cern.ch/user/a/akourkou/work/CSC_noDep  -no_cleanup $* >${cmtCSCntupletempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=CSCntuple -version=CSCntuple-r0 -path=/afs/cern.ch/user/a/akourkou/work/CSC_noDep  -no_cleanup $* >${cmtCSCntupletempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtCSCntupletempfile}
  unset cmtCSCntupletempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtCSCntupletempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtCSCntupletempfile}
unset cmtCSCntupletempfile
exit $cmtsetupstatus

