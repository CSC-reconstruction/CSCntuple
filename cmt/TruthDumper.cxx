#include "MuonIDNtupleMakers/TruthDumper.h"
#include "StoreGate/StoreGateSvc.h"
#include "muonEvent/Muon.h"

#include "MCTruthClassifier/MCTruthClassifierDefs.h"
#include "MCTruthClassifier/MCTruthClassifier.h"

#include "McParticleEvent/TruthParticleContainer.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <vector>


typedef  TruthParticle* TruthParticle_star;


TruthDumper::TruthDumper(const std::string& name, ISvcLocator* pSvcLocator):
  NtupleMakerBase(name, pSvcLocator),
  m_dumpFlag(false),
  m_doPreBremMuons(false),
  m_doPostBremMuons(true),
  m_doAddAllTruthParticles(false),
  m_MCTruthClassifier("MCTruthClassifier")
  //  m_MCTruthClassifier("MCTruthClassifier",this)
{
  declareProperty("TruthContainerKey",    m_TruthContainerKey);
  declareProperty("DoEventRecordDump",    m_dumpFlag = false);
  declareProperty("MuonMCStatus",         m_MCstatus = 1);
  declareProperty("DoPreBremMuons",       m_doPreBremMuons = false);
  declareProperty("AddAllTruthParticles", m_doAddAllTruthParticles = false);
  
  if(!m_doPreBremMuons){ m_writeKey="MuonFinalStateTruthParticles"; }
  else{  m_writeKey="MuonPreBremTruthParticles"; }
  
}

StatusCode TruthDumper::loadTools() {
  
  MsgStream log(msgSvc(), name());

  StatusCode sc = m_MCTruthClassifier.retrieve();
  if (sc.isSuccess()){
    log<<MSG::INFO << "Retrieved truth classifier " << m_MCTruthClassifier << endreq;
  }else{
    log<<MSG::FATAL<<"Could not get truth classifier " << m_MCTruthClassifier <<endreq; 
    return sc;
  }
  
  return StatusCode::SUCCESS;
}

StatusCode TruthDumper::addBranches() {
  
  addBranch("muonTruth_nMuon",   m_aan_muTruth_nMuons, "muonTruth_nMuon/i");
  addBranch("muonTruth_pt",      m_aan_muTruth_pt);
  addBranch("muonTruth_eta",     m_aan_muTruth_eta);     
  addBranch("muonTruth_phi",     m_aan_muTruth_phi);     
  addBranch("muonTruth_charge",  m_aan_muTruth_charge);  
  addBranch("muonTruth_PDGID",   m_aan_muTruth_PDGID); 
  addBranch("muonTruth_barcode", m_aan_muTruth_barcode);
  addBranch("muonTruth_type",    m_aan_muTruth_type);    
  addBranch("muonTruth_origin",  m_aan_muTruth_origin);  

  if(m_doAddAllTruthParticles) {
      addBranch("mc_barcode", m_aan_barcode);
      addBranch("mc_charge", m_aan_charge);
      addBranch("mc_child_barcode", m_aan_child_barcode);
      addBranch("mc_eta", m_aan_eta);
      addBranch("mc_m", m_aan_m);
      addBranch("mc_parent_barcode", m_aan_parent_barcode);
      addBranch("mc_pdgid", m_aan_pdgid);
      addBranch("mc_phi", m_aan_phi);
      addBranch("mc_pt", m_aan_pt);
      addBranch("mc_status", m_aan_status);
  }
  
  return StatusCode::SUCCESS;
  
}

StatusCode TruthDumper::clearBranches() {
  
  m_aan_muTruth_nMuons=0;
  m_aan_muTruth_pt->clear();
  m_aan_muTruth_eta->clear();
  m_aan_muTruth_phi->clear();
  m_aan_muTruth_charge->clear();
  m_aan_muTruth_PDGID->clear();
  m_aan_muTruth_barcode->clear();
  m_aan_muTruth_type->clear();
  m_aan_muTruth_origin->clear();
  
  if(m_doAddAllTruthParticles) {
      m_aan_barcode->clear();
      m_aan_charge->clear();
      m_aan_child_barcode->clear();
      m_aan_eta->clear();
      m_aan_m->clear();
      m_aan_parent_barcode->clear();
      m_aan_pdgid->clear();
      m_aan_phi->clear();
      m_aan_pt->clear();
      m_aan_status->clear();
  }
  
  return StatusCode::SUCCESS;
  
}

StatusCode TruthDumper::processNewJob() {
  
  // we only need to push_back the names of the
  // various met's once per job, so do it here
  //    m_aan_met_name->insert(m_aan_met_name->begin(), m_keys.begin(), m_keys.end());
  
  return StatusCode::SUCCESS;
  
}

StatusCode TruthDumper::processNewEvent() {

  MsgStream log(msgSvc(), name());
  
  StatusCode sc;
  
  const TruthParticleContainer *ReadCollection = 0;
  
  sc=this->evtStore()->retrieve( ReadCollection, m_TruthContainerKey);
  if( sc.isFailure()  ||  !ReadCollection) {
    log << MSG::WARNING << "Unable to open TruthContainer " << m_TruthContainerKey << endreq;
    return StatusCode::SUCCESS;
  }
  
  TruthParticleContainer *writeCollection = new TruthParticleContainer();
  sc = this->evtStore()->record(writeCollection,m_writeKey);
  if (sc.isFailure()) {
    log << MSG::ERROR << "Unable to record selected truth particles " 
	<< m_writeKey << " in StoreGate" << endreq;
    return StatusCode::SUCCESS;
  }

  
  if(m_doPreBremMuons) m_doPostBremMuons=false;

  // Dumpe event record in debug mode ///////////////////
  if(m_dumpFlag) ReadCollection->dump(std::cout);
  
  
  // set up the necessary variables///////////////////////
  TruthParticleContainer::const_iterator iterTP;
  TruthParticleContainer::const_iterator iterTP2;
  TruthParticle* truthParticle_temp = 0;
  TruthParticle* truthParticle_hold = 0;
  
  std::set<int> Stored_Muons;
  std::set<int>::iterator iterMU;
  
  int current_pdgID;
  int child_ID;
  unsigned int n_parents;
  int loopcount = 0;
  
  bool Muon_found  = false;
  bool wrong_parent = false;
  
  
  /////////////////////////////////////////////////////////////////////
  // loop through container and get muons 
  /////////////////////////////////////////////////////////////////////
  
  for(iterTP= ReadCollection->begin();iterTP != ReadCollection->end(); ++iterTP){
    current_pdgID = (*iterTP)->genParticle()->pdg_id();
    
    if( abs(current_pdgID) == 13  &&  (*iterTP)->nParents()>0){   
      
      
      //// set variables for this check
      child_ID  = current_pdgID;
      n_parents = (*iterTP)->nParents();
      truthParticle_temp = new TruthParticle( (**iterTP) );
      truthParticle_hold = new TruthParticle( (**iterTP) );
      loopcount    = 0;
      wrong_parent = false;
      
      //////////////////////////////////////////////////////////
      // check if parent is W, Z, or another muon.
      // if W or Z, great!
      // if other muon parent, only need first parent muon (others found in loop over children)
      //////////////////////////////////////////////////////////
      for(unsigned int i=0; i<(*iterTP)->nParents(); i++)
	{
	  if( (*iterTP)->genMother(i)->pdg_id()== current_pdgID) {wrong_parent = true;}
	  
	}
      
      
      
      //Loop through children and look for muon /////////////
      Muon_found = this->LoopDownThroughChildren(child_ID, m_MCstatus, loopcount, m_doPostBremMuons, truthParticle_temp);
      
      
      if(Muon_found && Stored_Muons.find( truthParticle_temp->genParticle()->barcode() ) == Stored_Muons.end() ){
	if(m_dumpFlag){log << MSG::INFO <<" Added one w/ barcode:: "<<truthParticle_temp->genParticle()->barcode() << endreq;}
	
	std::pair<unsigned int, unsigned int> Classify=m_MCTruthClassifier->particleTruthClassifier( truthParticle_temp->genParticle() );

	m_aan_muTruth_nMuons++;
	m_aan_muTruth_pt->push_back(      truthParticle_temp->pt()/1000. );
	m_aan_muTruth_eta->push_back(     truthParticle_temp->eta() );
	m_aan_muTruth_phi->push_back(     truthParticle_temp->phi() );
	m_aan_muTruth_charge->push_back(  truthParticle_temp->charge() );
	m_aan_muTruth_PDGID->push_back(   truthParticle_temp->genParticle()->pdg_id() );
	m_aan_muTruth_barcode->push_back( truthParticle_temp->genParticle()->barcode() );
	m_aan_muTruth_type->push_back(    Classify.first );
	m_aan_muTruth_origin->push_back(  Classify.second );

	writeCollection->push_back( new TruthParticle( (*truthParticle_temp) ) );
	Stored_Muons.insert( truthParticle_temp->genParticle()->barcode() );
      }
      
      
      delete truthParticle_temp;
      delete truthParticle_hold;
    }//if muon

    else if( (*iterTP)->genParticle()->status()==1){
      
    }
    
  }

  sc=this->evtStore()->setConst(writeCollection);
  if( sc.isFailure() ) {
    log << MSG::WARNING << "Unable to write TruthContainer " << m_writeKey << endreq;
    return StatusCode::SUCCESS;
  }

  if(m_doAddAllTruthParticles) {

      for(iterTP= ReadCollection->begin();iterTP != ReadCollection->end(); ++iterTP){

          TruthParticle *t = (*iterTP);

          m_aan_barcode->push_back(t->barcode());
          m_aan_charge->push_back(t->charge());
          m_aan_eta->push_back(t->eta());
          m_aan_m->push_back(t->m());
          m_aan_pdgid->push_back(t->genParticle()->pdg_id());
          m_aan_phi->push_back(t->phi());
          m_aan_pt->push_back(t->pt());
          m_aan_status->push_back(t->genParticle()->status());

          std::vector<int> childBarcodes;
          std::vector<int> parentBarcodes;

          for(unsigned int i = 0; i < t->nDecay(); i++)
              childBarcodes.push_back(t->child(i)->barcode());
          for(unsigned int i = 0; i < t->nParents(); i++)
              parentBarcodes.push_back(t->mother(i)->barcode());
          m_aan_child_barcode->push_back(childBarcodes);
          m_aan_parent_barcode->push_back(parentBarcodes);
      }
  }

  return StatusCode::SUCCESS;

}

/////////////////////////////////////////////////////////////////////////////

bool  TruthDumper::LoopDownThroughChildren(int child_ID, int status_check, int loopcount, bool doPostBrem,
        TruthParticle_star& truthParticle_temp){

    bool particle_found = true;
    bool exeptionalcase = false;
    bool foundit = false;

    TruthParticle* truthParticle_hold = 0;

    // first loop, to get last particle before brem
    while( truthParticle_temp->genParticle()->status() != status_check && particle_found==true && !exeptionalcase)
    {
        foundit = false;

        if(truthParticle_temp->nDecay()==0 && truthParticle_temp->genParticle()->status()>1){exeptionalcase = true;}
        for(unsigned int k=0; k<truthParticle_temp->nDecay(); k++){
            if( truthParticle_temp->child(k)->genParticle()->pdg_id()== child_ID && !foundit){
                loopcount++;
                truthParticle_hold = new TruthParticle( *(truthParticle_temp->child(k)) );
                delete truthParticle_temp;
                truthParticle_temp = new TruthParticle( (*truthParticle_hold) );
                delete truthParticle_hold;
                foundit=true;
            }
            else if(k==(truthParticle_temp->nDecay()-1) && !foundit){
                particle_found = false;

            }
        }
    }//end while loop //////////////

    // second loop to get past brem
    if(doPostBrem)
    {
        while (!exeptionalcase && particle_found &&
                truthParticle_temp->genParticle()->status() == status_check && 
                truthParticle_temp->nDecay() > 0)
        {
            foundit = false;
            for(unsigned int k=0; k<truthParticle_temp->nDecay(); k++){
                if( truthParticle_temp->child(k)->genParticle()->pdg_id()== child_ID && 
                        truthParticle_temp->child(k)->genParticle()->status() == status_check)
                {
                    loopcount++;
                    truthParticle_hold = new TruthParticle( *(truthParticle_temp->child(k)) );
                    delete truthParticle_temp;
                    truthParticle_temp = new TruthParticle( (*truthParticle_hold) );
                    delete truthParticle_hold;
                    foundit =true;
                }
                else if(k==(truthParticle_temp->nDecay()-1) && !foundit){
                    particle_found = false;
                }
            } //end for loop over children
        } //end while loop
    }// end if doPostBrem /////////

    if(exeptionalcase) particle_found = false;
    return(particle_found);
}

/////////////////////////////////////////////////////////////////////////////
