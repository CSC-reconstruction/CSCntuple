// CreateNtuple.cxx
// Author: Woochun Park ( woo.chun.park@cern.ch )
// Modified by Kalliopi Iordanidou (Kalliopi.Iordanidou@cern.ch)
// Modified by Athina Kourkoumeli-Charalampidi (Athina.Kourkoumeli-Charalampidi@cern.ch)
#include "CreateNtuple.h"

//#include "AthenaKernel/errorcheck.h"

#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ISvcLocator.h"



#include "TFile.h"
#include "TTree.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <TLorentzVector.h>

#include "MuonPrepRawData/CscPrepDataContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
//#include "​xAODTracking/TrackParticle.h"
#include "MuonRIO_OnTrack/CscClusterOnTrack.h"

#include "TrkCompetingRIOsOnTrack/CompetingRIOsOnTrack.h"

#include "Identifier/Range.h"
typedef Identifier::size_type   size_type;



typedef std::vector<const Trk::RIO_OnTrack*> RioList;

//////////////////////////////////////////////////////////////
CreateNtuple::CreateNtuple(const std::string& name, ISvcLocator* pSvcLocator)
: AthAlgorithm(name, pSvcLocator),
m_evtCnt(0),m_pmuon_detmgr(0),m_phelper(0),
m_helperTool("Muon::MuonEDMHelperTool/MuonEDMHelperTool"),
m_cluster_fitter("QratCscClusterFitter/QratCscClusterFitter"),
m_clusterTool("CscClusterUtilTool/CscClusterUtilTool"),
m_stripFitter("CalibCscStripFitter/CalibCscStripFitter"),
m_idHelperTool("Muon::MuonIdHelperTool/MuonIdHelperTool"),
m_idToFixedIdTool("MuonCalib::IdToFixedIdTool")
//m_muonSegmentConverterTool("Muon::MuonSegmentConverterTool/MuonSegmentConverterTool")
{
    cout<<"constructor called!"<<endl;
    // Declare user-defined prtperties from jobOpts - cuts and vertexing methods etc
    declareProperty("MuonCollectionName",   m_muonCollName = "Muons");
    declareProperty("outputNTupleName",     m_userFName);
    declareProperty("readMC",               m_readMC = true);
    declareProperty("readESD",              m_readESD);
    declareProperty("muonMomentumCut",      m_muonMomentumCut = 0.); // MeV
    declareProperty("dumpNtuple",           m_dumpNtuple = true);
    declareProperty("fillClusterBranches",  m_fillClusterBranches = false);
    declareProperty("fillSegmentBranches",  m_fillSegmentBranches = true);
    declareProperty("ContainerName",          m_containerName);//Athina
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
StatusCode CreateNtuple::initialize(){
    
    
    if( !m_readESD )
        m_fillClusterBranches = false;
    ATH_MSG_INFO( "Starting Athina's code");
    ATH_MSG_INFO( "in initialize()" );
    ATH_MSG_INFO("readMC?                  " << m_readMC );
    ATH_MSG_INFO("readESD?                  " << m_readESD );
    ATH_MSG_INFO("outputNTupleName?        " << m_userFName );
    ATH_MSG_INFO("dumpNtuple?              " << m_dumpNtuple );
    ATH_MSG_INFO("fillClusterBranches?     " << m_fillClusterBranches);
    ATH_MSG_INFO("fillSegmentBranches?     " << m_fillSegmentBranches);
    ATH_MSG_INFO("MuonCollection Name      " << m_muonCollName );
    
    
    string command = "RECREATE";
    cout<<"output ntuple name (Athina): "<<m_userFName<<endl;
    OutputFile = new TFile( m_userFName.c_str(), command.c_str() );
    
    // get the tool service
    IToolSvc* toolSvc;
    if( service("ToolSvc",toolSvc).isFailure() ){;
        ATH_MSG_ERROR("Unable to retrieve ToolSvc");
        return StatusCode::FAILURE;
    }
    
    //declareProperty( "SegmentContainerName", m_muonSegmentLocation = "MuonSegments" );
    //declareProperty( "xAODContainerName", m_containerName = "MuonSegments" );
    declareProperty("MuonSegmentLocation", m_muonSegmenteCollectionName = "MuonSegments" );
    
    
    ATH_MSG_INFO("Done with the property declaration");
    
    ATH_MSG_INFO("Initializing branches");
    initializeBranches();
    
    if( detStore()->retrieve(m_pmuon_detmgr).isFailure() ){
        ATH_MSG_ERROR(" Cannot retrieve MuonGeoModel ");
        return StatusCode::RECOVERABLE;
    }
    ATH_MSG_DEBUG("Retrieved geometry");
    
    m_phelper = m_pmuon_detmgr->cscIdHelper();
    
    CHECK( m_helperTool.retrieve() );
    CHECK( m_idHelperTool.retrieve() );
    CHECK( m_idToFixedIdTool.retrieve() );
    CHECK( m_clusterTool.retrieve() );
    CHECK( m_cluster_fitter.retrieve() );

    ATH_MSG_INFO("m_idHelperTool: "<<m_idHelperTool);
    
    return StatusCode::SUCCESS;
    
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
StatusCode CreateNtuple::execute() {
    
    
    ATH_MSG_INFO("clearing branches");
    
    clearBranches();
    
    // m_run and m_evt vector available with m_evtCnt
    if( getEventInfo().isFailure() )return StatusCode::FAILURE;
    
    ATH_MSG_INFO("next entry");
    
    /* // Retrieve the AOD particles:
     const Trk::SegmentCollection* segments = 0;
     if(evtStore()->transientContains<Trk::SegmentCollection>(m_muonSegmentLocation)) {
     if(evtStore()->retrieve(segments,m_muonSegmentLocation).isFailure()) {
     ATH_MSG_FATAL( "Unable to retrieve " << m_muonSegmentLocation );
     return StatusCode::FAILURE;
     }
     }*/
    //Create Muon
    //xAOD::Muon* muon = new xAOD::Muon();
    
    vector<CscSegmentInfo> assocSegments;
    
    const xAOD::MuonContainer* Muons = evtStore()->retrieve< const xAOD::MuonContainer >( m_muonCollName );
    if (!Muons) {
        ATH_MSG_WARNING ("Couldn't retrieve Muons container with key: " << m_muonCollName);
        return StatusCode::SUCCESS;
    }
    ATH_MSG_INFO("Retrieved muons " << Muons->size());
    
    //MUON LOOOP
    
    xAOD::MuonContainer::const_iterator muonItr;
    for( muonItr = Muons->begin(); muonItr != Muons->end(); muonItr++ ){
        
        //ATH_MSG_INFO("In Muon Loop");
        
        const xAOD::Muon *muon = *muonItr;
        if( muon->pt() < m_muonMomentumCut ) continue;
        if( m_dumpNtuple ) {
            
            double extrp = -999.;
            double extreta = -999.;
            double extrphi = -999.;
            const xAOD::TrackParticle* MuonExtrapolatedParticle = muon->trackParticle(xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);
            
            if (MuonExtrapolatedParticle != NULL){
                extrp = MuonExtrapolatedParticle->pt();
                extreta = MuonExtrapolatedParticle->eta();
                extrphi = MuonExtrapolatedParticle->phi();
            }
            
            m_trk_me_p->push_back(extrp);
            m_trk_me_eta->push_back(extreta);
            m_trk_me_phi->push_back(extrphi);
            
            double idp = -999.;
            double ideta = -999.;
            double idphi = -999.;
            const xAOD::TrackParticle* MuonIDParticle = muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
            if (MuonIDParticle != NULL){
                idp = MuonIDParticle->pt();
                ideta = MuonIDParticle->eta();
                idphi = MuonIDParticle->phi();
            }
            ATH_MSG_INFO("MuonIDParticle");
            m_trk_id_p->push_back(idp);
            m_trk_id_eta->push_back(ideta);
            m_trk_id_phi->push_back(idphi);
            
            m_muAuthors->push_back(muon->author());
            
            
            TLorentzVector tlv;
            
            Double_t pt = muon->pt();
            Double_t eta = muon->eta();
            Double_t phi = muon->phi();
            Double_t e = muon->e();
            tlv.SetPtEtaPhiE(pt,eta,phi,e);
	
            
            
            m_trkPx->push_back(tlv.Px());
            m_trkPy->push_back(tlv.Py());
            m_trkPz->push_back(tlv.Pz());
            m_trkP->push_back(tlv.P());
            m_trkPt->push_back(muon->pt());
            m_trkEta->push_back(muon->eta());
            
            
            const xAOD::TrackParticle* TrackParticle = muon->trackParticle(xAOD::Muon::Primary);
            if (TrackParticle != NULL){
                double chisq = TrackParticle->chiSquared();
                double dof   = TrackParticle->numberDoF();
                m_trkChisq->push_back(chisq);
                m_trkDoF->push_back(int(dof));
            }
            
            
            //const ElementLinkVector<Trk::SegmentCollection> link = muon->muonSegmentLink();
            //m_nMuSeg->push_back( link.end()-link.begin());
            
            uint8_t numberOfBLayerHits=0;
            if( !(*muonItr)->summaryValue(numberOfBLayerHits,xAOD::numberOfBLayerHits) ){
                ATH_MSG_INFO("unsuccessfully retrieved the integer value, numberOfBLayerHits");
            }
            uint8_t numberOfPixelHits=0;
            if( !(*muonItr)->summaryValue(numberOfPixelHits,xAOD::numberOfPixelHits) ){
                ATH_MSG_INFO("unsuccessfully retrieved the integer value, numberOfPixelHits");
            }
            uint8_t numberOfSCTHits=0;
            if( !(*muonItr)->summaryValue(numberOfSCTHits,xAOD::numberOfSCTHits) ){
                ATH_MSG_INFO("unsuccessfully retrieved the integer value, numberOfSCTHits");
            }
            uint8_t numberOfTRTHits=0;
            if( !(*muonItr)->summaryValue(numberOfTRTHits,xAOD::numberOfTRTHits) ){
                ATH_MSG_INFO("unsuccessfully retrieved the integer value, numberOfTRTHits");
            }
            uint8_t numberOfTRTHighThresholdHits=0;
            if( !(*muonItr)->summaryValue(numberOfTRTHighThresholdHits,xAOD::numberOfTRTHighThresholdHits) ){
                ATH_MSG_INFO("unsuccessfully retrieved the integer value, numberOfTRTHighThresholdHits");
            }
            
            m_nBLYHit->push_back(numberOfBLayerHits);
            m_nPixelHit->push_back(numberOfPixelHits);
            m_nSCTHit->push_back(numberOfSCTHits);
            m_nTRTHit->push_back(numberOfTRTHits);
            m_nTHTHit->push_back(numberOfTRTHighThresholdHits);
            //ATH_MSG_INFO("Done retrieving layers");
            
            n_allMuons++;
            
            const xAOD::TrackParticle* MuonCBParticle = muon->trackParticle(xAOD::Muon::CombinedTrackParticle);
            if (MuonCBParticle != NULL){
                
                n_CombMuons++;

		if (abs(muon->eta())<2) continue;
		    n_eta++;

                
                const Trk::Track* track = MuonCBParticle->track();//works due to: const Trk::Track* TrackParticle_v1::track()
                
                vector<CscHitInfo> hits;
                const DataVector<const Trk::TrackStateOnSurface>* tsosCollection = track->trackStateOnSurfaces();
                DataVector<const Trk::TrackStateOnSurface>::const_iterator iTsos = tsosCollection->begin();
                for( ; iTsos!=tsosCollection->end(); iTsos++ ){
                    
                    n_TsosCollMuons++;
                    
                    if( ! (*iTsos)->type(Trk::TrackStateOnSurface::Measurement) )continue;
                    
                    n_TsosTypeMuons++;
                    
                    // get measurement, continue if hit to be removed
                    const Trk::MeasurementBase* mesb = (*iTsos)->measurementOnTrack();
                    const Trk::RIO_OnTrack* rio  = dynamic_cast<const Trk::RIO_OnTrack*>(mesb);
                    const Trk::CompetingRIOsOnTrack* crio = dynamic_cast<const Trk::CompetingRIOsOnTrack*>(mesb);
                    if( !rio && crio )rio = &crio->rioOnTrack(0);
                    if( !rio ) continue;
                    
                    n_RIO_OnTrackMuons++;
                    
		   Identifier detId = rio->detectorElement()->identify();

           
		   


		   ATH_MSG_INFO("detId "<<detId<<" isCSC "<<m_idHelperTool->isCsc(detId)<<" isMDT "<<m_idHelperTool->isMdt(detId)<<" isTGC "<<m_idHelperTool->isTgc(detId));

                    if (m_idHelperTool->isMuon(detId)) n_idHelperToolMuons1++;
		    if (m_idHelperTool->isCsc(detId)) n_idHelperToolMuons2++;
		    if (m_idHelperTool->isMdt(detId)) n_idHelperToolMuons3++;
		    if (m_idHelperTool->isTgc(detId)) n_idHelperToolMuons4++;
                    
                    if( !m_idHelperTool->isMuon(detId) || !m_idHelperTool->isCsc(detId) )continue;
                    
                    
                    const Trk::TrackParameters* trackPars = (*iTsos)->trackParameters();
                    if ( !trackPars && m_readESD ) continue;
                    
                    n_trackParsMuons++;
                    
                    const Muon::CscClusterOnTrack* cluster = dynamic_cast<const Muon::CscClusterOnTrack*>(rio);
                    const Muon::CscPrepData* prep = dynamic_cast<const Muon::CscPrepData*> (cluster->prepRawData());
                    
                    Identifier hitId = cluster->identify();
                    int istation = m_phelper->stationName(hitId) - 49;
                    string chamber_name = (istation-1) ? "CSS" : "CSL";
                    
                    int zsec = m_phelper->stationEta(hitId);
                    int phisec = m_phelper->stationPhi(hitId);
                    int sector = m_phelper->sector(hitId);
                    
                    int wlay = m_phelper->wireLayer(hitId);
                    int measphi = m_phelper->measuresPhi(hitId);
                    string etaphi = (measphi) ? "PhiHit" : "EtaHit";
                    int istrip = m_phelper->strip(hitId); //[1,192]// - 1;
                    
                    int status = cluster->status();
                    CscHitInfo hit(sector, wlay, measphi, istrip, status);
                    hits.push_back(hit);
                    
                    
                    ATH_MSG_VERBOSE("    RIO " << chamber_name << "[" << zsec << ", " << phisec << "] "
                                    << sector << " layer:measphi:istrip " << wlay << ":" << etaphi );
                    
                    m_hitToMuon->push_back( int(m_muAuthors->size() -1) );
                    m_hitToSeg->push_back( int(m_assocSegToMuon->size() -1) );
                    
                    m_hitSector->push_back(sector);
                    m_hitWlay->push_back(wlay);
                    m_hitMeasphi->push_back(measphi);
                    m_hitIstrip->push_back(istrip);
                    
                    m_hitTime->push_back(cluster->time());
                    m_hitSfit->push_back(status);
                    m_hitTfit->push_back(cluster->timeStatus());
                    
                    
                    if( prep )m_hitCharge->push_back(prep->charge());
                    
                    const std::vector<Identifier>& strip_ids = prep->rdoList();
                    m_hitNstrip->push_back(strip_ids.size());
                    m_hitStrip0->push_back(m_phelper->strip(*strip_ids.begin()));
                    
                    m_hitPos->push_back( prep->localPosition()[Trk::loc1] );
                    m_hitDPos->push_back( Amg::error(prep->localCovariance(),Trk::loc1) );//return diagonal error of the matrix. caller should ensure the matrix is symmetric and the index is in range
                    
                    
                    std::vector<double> charges;
                    ICscClusterFitter::StripFitList sfits;
                    
                    std::vector<const Muon::CscStripPrepData*> strips = m_clusterTool->getStrips(prep);
                    ATH_MSG_INFO("m_clusterTool "<<m_clusterTool<<" prep "<<prep);
                    ATH_MSG_INFO("strips size "<<strips.size());
                    for (unsigned int s=0; s<strips.size(); ++s) {
                        ICscClusterFitter::StripFit sfit;
                        sfit = m_stripFitter->fit(*strips[s]);
                        ATH_MSG_VERBOSE ( " in loop charge " << sfit.charge );
                        sfits.push_back(sfit);
                    }
                    //m_clusterTool->getStripFits(prep, sfits);
                    
                    ATH_MSG_INFO("sfits size "<<sfits.size());
                    
                    for( uint i = 0; i < sfits.size(); i++ ) charges.push_back(sfits[i].charge);
                    
                    std::vector<double, std::allocator<double> >::iterator pkit = std::max_element( charges.begin(), charges.end() );
                    
                    int str0ToPstr = pkit -charges.begin();
                    
                    float qleft = ( pkit > charges.begin() ) ? *( pkit-1 ): 0;
                    float qright = (pkit+1 < charges.end() )  ? *( pkit+1 ): 0;
                    
                    float dqleft = ( pkit > charges.begin() ) ? sfits[str0ToPstr-1].dcharge: -1;
                    float dqright = (pkit+1 < charges.end() )  ? sfits[str0ToPstr+1].dcharge: -1;
                    
                    m_hitQpeak->push_back(*pkit);
                    m_hitQleft->push_back(qleft);
                    m_hitQright->push_back(qright);
                    m_hitDqpeak->push_back(sfits[str0ToPstr].dcharge);
                    m_hitDqleft->push_back(dqleft);
                    m_hitDqright->push_back(dqright);
                    m_hitPhase->push_back(sfits[0].phase);
                    
                    m_hitRecoTime->push_back(sfits[str0ToPstr].time);
                    m_hitbeforeT0CorrTime->push_back(sfits[str0ToPstr].time_beforeT0Corr);
                    m_hitbeforeBPCorrTime->push_back(sfits[str0ToPstr].time_beforeBPCorr);
                    
                    if( !measphi ){
                        std::vector<ICscClusterFitter::Result> results = m_cluster_fitter->fit(sfits);
                        ICscClusterFitter::DataMap dmap;
                        dmap = results[0].dataMap;
                        
                        m_hitPosrefit->push_back(results[0].position);
                        m_hitDposrefit->push_back(results[0].dposition);
                        m_hitQfitdiff->push_back(dmap["scordiff"]);
                        m_hitSfitrefit->push_back(results[0].clusterStatus);
                        
                        if( dmap["scordiff"] == 0.0 ) m_hitQfitsig->push_back(999);
                        else m_hitQfitsig->push_back( dmap["scordiff"]/dmap["dscordiff"] );
                    }
                    else{
                        m_hitPosrefit->push_back(999.);
                        m_hitDposrefit->push_back(999.);
                        m_hitQfitdiff->push_back(999.);
                        m_hitSfitrefit->push_back(1);
                        m_hitQfitsig->push_back(999);
                    }
                    
                }//tsos collection
                
                int prevSector = 0;
                vector<CscHitInfo> sameHits;
                for( uint i = 0; i < hits.size(); i++ ){
                    
                    if( i == 0 ){
                        sameHits.push_back(hits[i]);
                        prevSector = hits[i].sector;
                    }
                    else if( i+1 ==hits.size() ){
                        sameHits.push_back(hits[i]);
                        CscSegmentInfo seg(m_muAuthors->size()-1, prevSector, sameHits);
                        ATH_MSG_VERBOSE(" SegmuonIdx:  " << m_muAuthors->size()-1);
                        assocSegments.push_back(seg);
                    }
                    else{
                        if( prevSector != hits[i].sector ){
                            CscSegmentInfo seg(m_muAuthors->size()-1, prevSector, sameHits);
                            assocSegments.push_back(seg);
                            sameHits.clear();
                            sameHits.push_back(hits[i]);
                            prevSector = hits[i].sector;
                        }
                        else{
                            sameHits.push_back(hits[i]);
                            prevSector = hits[i].sector;
                        }
                    }
                }//hits
                for( uint j = 0; j < hits.size(); j++ ){
                    ATH_MSG_VERBOSE("     =>>>>  " << j << " " << hits[j].sector<< " " << hits[j].wlay << " " << hits[j].measphi
                                    << " " << hits[j].istrip);
                }
                
                
                for( uint i = 0; i < assocSegments.size(); ++i ){
                    ATH_MSG_VERBOSE( "  my found segments " << i << " " << assocSegments[i].muonIdx << " ");
                    
                    vector<CscHitInfo> hits = assocSegments[i].hits;
                    for( uint j = 0; j < hits.size(); j++ ){
                        ATH_MSG_VERBOSE("     ====>  " << j << " " << hits[j].sector<< " " << hits[j].wlay << " " << hits[j].measphi
                                        << " " << hits[j].istrip);
                    }
                }//assocSegments
                
                
                
                
                
            }//MuonCBParticle
            ATH_MSG_INFO("Done dumpNtuple");
            
        }//m_dumpNtuple
        
    }//end of muon loop
    
    //*******************************************************//
    
    // Create the xAOD container and its auxiliary store
    
    const xAOD::MuonSegmentContainer* segments = 0;
    ATH_CHECK(evtStore()->retrieve(segments,m_muonSegmenteCollectionName));
    ATH_MSG_INFO("Retrieved SegmentContainer " << m_muonSegmenteCollectionName << " size " << segments->size() );
    
    //************ TRK MUON SEGMENTS ***********************//
    
    const Trk::SegmentCollection *p_muonSegmentContainer = 0;
    if( evtStore()->retrieve(p_muonSegmentContainer, "MuonSegments").isFailure() ){
        ATH_MSG_FATAL ("No MuonSegmentContainer found in StoreGate!");
        return StatusCode::FAILURE;
    }
    
    n_assocSegments+=assocSegments.size();
    n_Segments+=p_muonSegmentContainer->size();
    
    //Fill th Segments Associated to muons for the muons ntuple
    vector<uint> theIdxes;
    ATH_MSG_VERBOSE("assocSegments.size(). " << assocSegments.size() << " Trk::SegmentCollection size " << p_muonSegmentContainer->size());
    
    for( uint iaseg = 0; iaseg < assocSegments.size(); iaseg++ ){
        int reference = 0;
        uint theSegmentIdx = 999;
        ATH_MSG_VERBOSE("assocSegmentInfo .. " << iaseg << " " << assocSegments[iaseg].muonIdx << " "
                        << assocSegments[iaseg].sector << " " << assocSegments[iaseg].hits.size() );
        
        for( uint i = 0; i < p_muonSegmentContainer->size(); i++ ){
            //ATH_MSG_INFO("Inside muon segment container loop ");
            const Muon::MuonSegment* mSeg = dynamic_cast<const Muon::MuonSegment*> (p_muonSegmentContainer->at(i));
            
            Identifier id = m_helperTool->chamberId(*mSeg);
            if( !id.is_valid() )continue;
            //ATH_MSG_INFO("Chamber ID is valid");
            if( !m_idHelperTool->isMuon(id) )continue;
            //ATH_MSG_INFO("isMuon");
            MuonCalib::MuonFixedId fid = m_idToFixedIdTool->idToFixedId( id ) ;
            int stationName = fid.stationName();
            
            //ATH_MSG_INFO("station name "<<stationName);
            if( stationName != 33 && stationName != 34 ) continue;//33,34 for CSC, 15,24 for MDT
            
            vector<CscHitInfo> hits;
            //ATH_MSG_INFO("mSeg->numberOfContainedROTs() " << mSeg->numberOfContainedROTs());
            for( uint irio = 0 ; irio < mSeg->numberOfContainedROTs(); irio++ ){
                const Trk::RIO_OnTrack* rot = mSeg->rioOnTrack(irio);
                const Muon::CscClusterOnTrack* cluster = dynamic_cast<const Muon::CscClusterOnTrack*>(rot);
                const Muon::CscPrepData* prep = dynamic_cast<const Muon::CscPrepData*> (cluster->prepRawData());
                
                Identifier hitId = cluster->identify();
                int wlay = m_phelper->wireLayer(hitId);
                int measphi = m_phelper->measuresPhi(hitId);
                int istrip = m_phelper->strip(hitId); //[1,192]// - 1;
                int sector = m_phelper->sector(hitId);
                CscHitInfo hit(sector, wlay, measphi, istrip, cluster->status());
                hits.push_back(hit);
                ATH_MSG_DEBUG("wlay " << wlay<<" measphi "<<measphi<<" istrip "<<istrip<<" number of hits"<<hits.size());
                
                
            }
            
            int cntMatched = getNumberOfMatchedHits(assocSegments[iaseg], hits);
            if( cntMatched > reference ){
                reference = cntMatched;
                theSegmentIdx = i;
            }
            
            ATH_MSG_VERBOSE("Matching Progress..sgIdx:muonSegIdx:matchingIdx:reference " << iaseg << " " << i << "  " << theSegmentIdx << " " << reference  );
            
        }
        theIdxes.push_back(theSegmentIdx);
        
    } //assocSegments
    
    
    //Fill AssocTrees
    //Fill the muon tree quantities
    if( !theIdxes.empty() ){
        
        for( uint iaseg =0 ; iaseg < assocSegments.size(); iaseg++ ){
            int theSegmentIdx = theIdxes[iaseg];
            if( theSegmentIdx > 990 )continue;
            
            ATH_MSG_VERBOSE(" assoc seg  " << iaseg << " " <<  theSegmentIdx << " " << p_muonSegmentContainer->size() << " " );
            
            const Muon::MuonSegment* theSeg = dynamic_cast<const Muon::MuonSegment*> (p_muonSegmentContainer->at(theSegmentIdx));
            
            m_assocSegToMuon->push_back( assocSegments[iaseg].muonIdx );
            m_assocSegTime->push_back( theSeg->time() );
            m_assocSegChisq->push_back( theSeg->fitQuality()->chiSquared());
            m_assocSegNDoF->push_back( theSeg->fitQuality()->numberDoF());
            
            m_assocSegGposx->push_back( theSeg->globalPosition().x() );
            m_assocSegGposy->push_back( theSeg->globalPosition().y() );
            m_assocSegGposz->push_back( theSeg->globalPosition().z() );
            
            m_assocSegPosTheta->push_back( theSeg->globalPosition().theta() );
            m_assocSegPosEta->push_back( theSeg->globalPosition().eta() );
            m_assocSegPosPhi->push_back( theSeg->globalPosition().phi() );
            
            m_assocSegGdirx->push_back( theSeg->globalDirection().x() );
            m_assocSegGdiry->push_back( theSeg->globalDirection().y() );
            m_assocSegGdirz->push_back( theSeg->globalDirection().z() );
            
            m_assocSegDirTheta->push_back( theSeg->globalDirection().theta() );
            m_assocSegDirEta->push_back( theSeg->globalDirection().eta() );
            m_assocSegDirPhi->push_back( theSeg->globalDirection().phi() );
            
            m_assocSegEtalocpos->push_back(theSeg->localParameters()[Trk::locY]);
            m_assocSegPhilocpos->push_back(theSeg->localParameters()[Trk::locX]);
            m_assocSegEtalocdir->push_back(theSeg->localDirection().angleYZ());
            m_assocSegPhilocdir->push_back(theSeg->localDirection().angleXZ());
            
            m_assocSegdy->push_back( Amg::error(theSeg->localCovariance(),Trk::locX) );
            m_assocSegdz->push_back( Amg::error(theSeg->localCovariance(),Trk::locY) );
            m_assocSegday->push_back( Amg::error(theSeg->localCovariance(),Trk::locZ) );
            
            int segSector = 0;
            
            vector<int> assocPstrips;
            vector<float> assocCharges, assocTimes, assocPosesX, assocDPoses;
            vector<float> assocPosesY, assocPosesZ;
            vector<int> assocSfits;
            
            for( int i = 0; i < 8; ++i ){
                assocPstrips.push_back(-1);
                assocSfits.push_back(-1);
                assocCharges.push_back(-9.);
                assocTimes.push_back(-9999.);
                assocPosesX.push_back(-999.);
                assocPosesY.push_back(-999.);
                assocPosesZ.push_back(-999.);
                assocDPoses.push_back(-999.);
            }
            
            for( uint irio = 0; irio < theSeg->numberOfContainedROTs(); irio++ ){
                const Trk::RIO_OnTrack* rot = theSeg->rioOnTrack(irio);
                const Muon::CscClusterOnTrack* cluster = dynamic_cast<const Muon::CscClusterOnTrack*>(rot);
                const Muon::CscPrepData* prep = dynamic_cast<const Muon::CscPrepData*> (cluster->prepRawData());
                
                Identifier hitId = cluster->identify();
                segSector = m_phelper->sector(hitId);
                int wlay = m_phelper->wireLayer(hitId);
                int measphi = m_phelper->measuresPhi(hitId);
                int istrip = m_phelper->strip(hitId); //[1,192]// - 1;
                
                const Trk::PlaneSurface& ssrf = (*theSeg).associatedSurface();
                const Amg::MatrixX& cov = rot->localCovariance();
                int dim = cov.rows();
                Trk::ParamDefs iloc = Trk::loc1;
                Trk::ParamDefs ierr = dim==1 ? Trk::loc1 : iloc;
                double d = Amg::error(cov,ierr);
                assocDPoses[measphi*4+wlay-1] = d;
                
                const Amg::Vector3D& gpos = rot->globalPosition();
                assocPosesX[measphi*4+wlay-1] = (ssrf.transform().inverse()*gpos).x();
                assocPosesY[measphi*4+wlay-1] = (ssrf.transform().inverse()*gpos).y();
                assocPosesZ[measphi*4+wlay-1] = (ssrf.transform().inverse()*gpos).z();
                
                assocPstrips[measphi*4+wlay-1] = istrip;
                if( prep ) assocCharges[measphi*4+wlay-1] = prep->charge();
                
                assocTimes[measphi*4+wlay-1] = cluster->time();
                assocSfits[measphi*4+wlay-1] = int(cluster->status());
            }
            
            m_assocSegSector->push_back( segSector );
            m_assocSegPstrips->push_back(assocPstrips);
            m_assocSegCharges->push_back(assocCharges);
            m_assocSegTimes->push_back(assocTimes);
            m_assocSegSfits->push_back(assocSfits);
            
            m_assocSegPosesX->push_back(assocPosesX);
            m_assocSegPosesY->push_back(assocPosesY);
            m_assocSegPosesZ->push_back(assocPosesZ);
            m_assocSegDPoses->push_back(assocDPoses);
            
        }
    }
    
    
    //ATH_MSG_INFO("Filling Segment Trees");
    //Fill Segment Trees
    for( uint i = 0; i < p_muonSegmentContainer->size(); i++ ){
        const Muon::MuonSegment* mSeg = dynamic_cast<const Muon::MuonSegment*> (p_muonSegmentContainer->at(i));
        
        Identifier id = m_helperTool->chamberId(*mSeg);
        if( !id.is_valid() )continue;
        if( !m_idHelperTool->isMuon(id) )continue;
        //ATH_MSG_INFO("Pass id");
        
        MuonCalib::MuonFixedId fid = m_idToFixedIdTool->idToFixedId( id ) ;
        int stationName = fid.stationName();
        
        if( stationName != 33 && stationName != 34 ) continue;
        //ATH_MSG_INFO("Pass stationName");
        int zsec = fid.eta();
        int phisec = fid.phi();
        int station = (stationName==33) ? 1 : 2;
        int sector = zsec*(2*phisec-station+1);
        
        short theMuonIdx = -1;
        for( uint iaseg = 0 ; iaseg<assocSegments.size(); iaseg++ ){
            if( theIdxes[iaseg] == i ) theMuonIdx = int(assocSegments[iaseg].muonIdx);
        }
        
        //ATH_MSG_VERBOSE(" theMuonIdx:  " << theMuonIdx);
        
        m_segToMuon->push_back( theMuonIdx );
        m_segSector->push_back( sector );
        m_segTime->push_back( mSeg->time());
        m_segx->push_back( (mSeg->globalPosition()).x() );
        m_segy->push_back( (mSeg->globalPosition()).y() );
        m_segz->push_back( (mSeg->globalPosition()).z() );
        
        m_segdirx->push_back( (mSeg->globalDirection()).x() );
        m_segdiry->push_back( (mSeg->globalDirection()).y() );
        m_segdirz->push_back( (mSeg->globalDirection()).z() );
        m_segPhi->push_back( (mSeg->globalDirection()).phi() );
        m_segEta->push_back( (mSeg->globalDirection()).eta() );
        
        vector<int> pstrips;
        vector<float> charges, times;
        vector<float> segPosesX, segPosesY, segPosesZ, segDPoses;
        vector<int> segSfits;
        for( int i = 0; i < 8; i++ ){
            pstrips.push_back(-1);
            charges.push_back(-9.);
            times.push_back(-9999.);
            segPosesX.push_back(-999.);
            segPosesY.push_back(-999.);
            segPosesZ.push_back(-999.);
            segSfits.push_back(-1);
            segDPoses.push_back(-999.);
        }
        
        for( uint irio = 0; irio < mSeg->numberOfContainedROTs(); irio++ ){
            const Trk::RIO_OnTrack* rot = mSeg->rioOnTrack(irio);
            const Muon::CscClusterOnTrack* cluster = dynamic_cast<const Muon::CscClusterOnTrack*>(rot);
            const Muon::CscPrepData* prep = dynamic_cast<const Muon::CscPrepData*> (cluster->prepRawData());
            
            Identifier hitId = cluster->identify();
            int wlay = m_phelper->wireLayer(hitId);
            int measphi = m_phelper->measuresPhi(hitId);
            int istrip = m_phelper->strip(hitId); //[1,192]// - 1;
            
            pstrips[measphi*4+wlay-1] = istrip;
            times[measphi*4+wlay-1] = cluster->time();
            if( prep )charges[measphi*4+wlay-1] = prep->charge();
            
            //Athina's addition
            const Trk::PlaneSurface& ssrf2 = (*mSeg).associatedSurface();
            const Amg::Vector3D& gpos2 = rot->globalPosition();
            segPosesX[measphi*4+wlay-1] = (ssrf2.transform().inverse()*gpos2).x();
            segPosesY[measphi*4+wlay-1] = (ssrf2.transform().inverse()*gpos2).y();
            segPosesZ[measphi*4+wlay-1] = (ssrf2.transform().inverse()*gpos2).z();
            
            segSfits[measphi*4+wlay-1] = int(cluster->status());
            
            
            const Amg::MatrixX& cov = rot->localCovariance();
            int dim = cov.rows();
            Trk::ParamDefs iloc = Trk::loc1;
            Trk::ParamDefs ierr = dim==1 ? Trk::loc1 : iloc;
            double d = Amg::error(cov,ierr);
            segDPoses[measphi*4+wlay-1] = d;
            
        }
        
        m_segPstrips->push_back(pstrips);
        m_segCharges->push_back(charges);
        m_segTimes->push_back(times);
        
        m_SegPosesX->push_back(segPosesX);
        m_SegPosesY->push_back(segPosesY);
        m_SegPosesZ->push_back(segPosesZ);
        
        m_SegSfits->push_back(segSfits);
        m_SegDPoses->push_back(segDPoses);
        
    }
    
    //ATH_MSG_INFO("Done with MuonSegments");
    
    m_evtCnt++;     // Increment event counter
    
    
    //ATH_MSG_INFO ( " before fillSegmentBr ");
    
    if( m_fillSegmentBranches ){
        m_segTree->Fill();
    }
    
    m_recoTree->Fill();
    
    //ATH_MSG_INFO ( " after fillSegment ");
    
    
    if( m_fillClusterBranches ){
        if( fillClusterBranches().isFailure() ) return StatusCode::FAILURE;
        m_cluTree->Fill();
    }
    
    ATH_MSG_INFO("end of this entry");
    
    return StatusCode::SUCCESS;
    
}


//*************************************************
StatusCode CreateNtuple::finalize(){
    
    // Summary of analysis can go here
    ATH_MSG_DEBUG ( "in finalize()" );
    
    std::cout << " " << std::endl;
    std::cout << "===================" << std::endl;
    std::cout << "SUMMARY OF ANALYSIS" << std::endl;
    std::cout << "===================" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "Total number of events analysed: " << m_evtCnt << std::endl;
    std::cout << " " << std::endl;
    
    OutputFile->cd();
    std::cout << "Total number of events analysed: " << m_evtCnt << std::endl;

    std::cout << " " << std::endl;
    std::cout << "===================" << std::endl;
    std::cout << "    CUT FLOW       " << std::endl;
    std::cout << "===================" << std::endl;
    std::cout << " " << std::endl;
    std::cout << "All muons          : " << n_allMuons << std::endl;
    std::cout << "Combined muons     : " << n_CombMuons << std::endl;
    std::cout << "pass eta           : " << n_eta << std::endl;
    std::cout << "tsos coll          : " << n_TsosCollMuons << std::endl;
    std::cout << "tsos type          : " << n_TsosTypeMuons << std::endl;
    std::cout << "RIO on tracks      : " << n_RIO_OnTrackMuons << std::endl;
    std::cout << "	isMuon             : " << n_idHelperToolMuons1 << std::endl;
    std::cout << "	isCSC              : " << n_idHelperToolMuons2 << std::endl;
    std::cout << "	isMDT              : " << n_idHelperToolMuons3 << std::endl;
    std::cout << "	isTGC              : " << n_idHelperToolMuons4 << std::endl;
    std::cout << "track parameters   : " << n_trackParsMuons << std::endl;  
    std::cout << " " << std::endl;
    std::cout << "all assoc segments   : " << n_assocSegments << std::endl;
    std::cout << "all  segments   : " << n_Segments << std::endl;


    
    if(m_fillSegmentBranches){
        m_segTree->Write();
        m_segTree->Print();
    }
    
    if( m_fillClusterBranches ){
        m_cluTree->Write();
        m_cluTree->Print();
    }
    
    m_recoTree->Write();
    m_recoTree->Print();
    
    OutputFile->Close();
    
    return StatusCode::SUCCESS;
}
//********************************************
void CreateNtuple::initializeBranches(void){
    
    n_allMuons=0;
    n_CombMuons=0;
    n_TsosCollMuons=0;
    n_TsosTypeMuons=0;
    n_RIO_OnTrackMuons=0;
    n_eta=0;
    n_idHelperToolMuons1=0;
    n_idHelperToolMuons2=0;
    n_idHelperToolMuons3=0;
    n_idHelperToolMuons4=0;
    n_trackParsMuons=0;
    n_assocSegments=0;
    n_Segments=0;
    
    m_recoTree = new TTree("muon","Muon Track Information");
    
    
    m_muAuthors = new std::vector<uint16_t>;
    m_trkPx = new std::vector<float>;
    m_trk_me_p = new std::vector<float>;
    m_trk_me_eta = new std::vector<float>;
    m_trk_me_phi = new std::vector<float>;
    m_trk_id_p = new std::vector<float>;
    m_trk_id_eta = new std::vector<float>;
    m_trk_id_phi = new std::vector<float>;
    
    m_trkPy = new std::vector<float>;
    m_trkPz = new std::vector<float>;
    m_trkP = new std::vector<float>;
    m_trkPt = new std::vector<float>;
    m_trkEta = new std::vector<float>;
    
    m_trkPVXQoverP = new std::vector<float>;
    m_trkPVXd0 = new std::vector<float>;
    m_trkPVXZ0 = new std::vector<float>;
    m_trkPVXTheta = new std::vector<float>;
    m_trkPVXPhi0 = new std::vector<float>;
    
    m_trkChisq = new std::vector<float>   ;
    m_trkDoF = new std::vector<uint8_t> ;
    
    m_nBLYHit = new std::vector<uint8_t>;
    m_nPixelHit = new std::vector<uint8_t>;
    m_nSCTHit = new std::vector<uint8_t>;
    m_nTRTHit = new std::vector<uint8_t>;
    m_nTHTHit = new std::vector<uint8_t>;
    
    m_nMDTHit = new std::vector<uint8_t>;
    m_nTGCPHit = new std::vector<uint8_t>;
    m_nTGCEHit = new std::vector<uint8_t>;
    m_nRPCPHit = new std::vector<uint8_t>;
    m_nRPCEHit = new std::vector<uint8_t>;
    m_nCSCPHit = new std::vector<uint8_t>;
    m_nCSCEHit = new std::vector<uint8_t>;
    m_nMuSeg = new std::vector<uint8_t>;
    
    m_hitToMuon = new std::vector<uint8_t>;
    m_hitSector = new std::vector<short>;
    m_hitWlay = new std::vector<uint8_t>;
    m_hitMeasphi = new std::vector<bool>;
    m_hitIstrip = new std::vector<uint8_t>;
    
    m_hitRecoTime = new std::vector<float>;
    m_hitbeforeT0CorrTime = new std::vector<float>;
    m_hitbeforeBPCorrTime = new std::vector<float>;
    
    m_hitTime = new std::vector<float>;
    m_hitSfit = new std::vector<uint8_t>;
    m_hitTfit = new std::vector<uint8_t>;
    m_hitPos = new std::vector<float>;
    m_hitPhiReFit = new std::vector<float>;
    m_hitDPos = new std::vector<float>;
    
    if( m_readESD ){
        m_hitCharge = new std::vector<float>;
        m_hitNstrip = new std::vector<uint8_t>;
        m_hitStrip0 = new std::vector<uint8_t>;
        
        m_hitQpeak = new std::vector<float>;
        m_hitQleft = new std::vector<float>;
        m_hitQright = new std::vector<float>;
        m_hitDqpeak = new std::vector<float>;
        m_hitDqleft = new std::vector<float>;
        m_hitDqright = new std::vector<float>;
        m_hitPhase = new std::vector<bool>;
        
        m_hitSfitrefit = new std::vector<uint8_t>;
        m_hitPosrefit = new std::vector<float>;
        m_hitPhiPosrefit = new std::vector<float>;
        m_hitDposrefit = new std::vector<float>;
        m_hitQfitsig = new std::vector<float>;
        m_hitQfitdiff = new std::vector<float>;
    }
    
    m_assocSegToMuon = new std::vector<uint8_t>;
    m_assocSegTime = new std::vector<float>;
    m_assocSegChisq = new std::vector<float>;
    m_assocSegNDoF = new std::vector<float>;
    m_assocSegGposx = new std::vector<float>;
    m_assocSegGposy = new std::vector<float>;
    m_assocSegGposz = new std::vector<float>;
    m_assocSegGdirx = new std::vector<float>;
    m_assocSegGdiry = new std::vector<float>;
    m_assocSegGdirz = new std::vector<float>;
    m_assocSegPosTheta = new std::vector<float>;
    m_assocSegPosEta = new std::vector<float>;
    m_assocSegPosPhi = new std::vector<float>;
    m_assocSegDirTheta = new std::vector<float>;
    m_assocSegDirEta = new std::vector<float>;
    m_assocSegDirPhi = new std::vector<float>;
    m_assocSegEtalocpos = new std::vector<float>;
    m_assocSegPhilocpos = new std::vector<float>;
    m_assocSegEtalocdir = new std::vector<float>;
    m_assocSegPhilocdir = new std::vector<float>;
    m_assocSegdy = new std::vector<float>;
    m_assocSegdz = new std::vector<float>;
    m_assocSegday = new std::vector<float>;
    m_assocSegdaz = new std::vector<float>;
    m_assocSegeyz = new std::vector<float>;
    m_assocSegeyay = new std::vector<float>;
    m_assocSegeyaz = new std::vector<float>;
    m_assocSegezay = new std::vector<float>;
    m_assocSegezaz = new std::vector<float>;
    m_assocSegeayaz = new std::vector<float>;
    
    m_hitToSeg = new std::vector<uint8_t>;
    m_assocSegSector = new std::vector<short>;
    
    m_assocSegSfits = new std::vector< std::vector<int> >;
    m_assocSegPstrips = new std::vector< std::vector<int> >;
    m_assocSegCharges = new std::vector< std::vector<float> >;
    m_assocSegTimes = new std::vector< std::vector<float> >;
    m_assocSegPosesX = new std::vector< std::vector<float> >;
    m_assocSegPosesY = new std::vector< std::vector<float> >;
    m_assocSegPosesZ = new std::vector< std::vector<float> >;
    m_assocSegDPoses = new std::vector< std::vector<float> >;
    
    if( m_fillClusterBranches ){
        m_cluToMuon = new std::vector<short>;
        
        m_cluMeasphi = new std::vector<bool>;
        m_cluWlay = new std::vector<uint8_t>;
        m_cluSector = new std::vector<short>;
        
        m_cluPstrip = new std::vector<uint8_t>;
        m_cluNstrip = new std::vector<uint8_t>;
        m_cluStrip0 = new std::vector<uint8_t>;
        
        m_cluPos  = new std::vector<float>;
        m_cluDpos = new std::vector<float>;
        m_cluSfit = new std::vector<uint8_t>;
        m_cluTfit = new std::vector<uint8_t>;
        m_cluTime = new std::vector<float>;
        m_cluQsum = new std::vector<float>;
        m_cluQpeak = new std::vector<float>;
        m_cluQleft = new std::vector<float>;
        m_cluQright = new std::vector<float>;
        
        m_clux = new std::vector<float>;
        m_cluy = new std::vector<float>;
        m_cluz = new std::vector<float>;
        m_clur = new std::vector<float>;
        
        m_cluDqpeak = new std::vector<float>;
        m_cluDqleft = new std::vector<float>;
        m_cluDqright = new std::vector<float>;
        m_cluPosrefit = new std::vector<float>;
        m_cluDposrefit = new std::vector<float>;
        m_cluQfitdiff = new std::vector<float>;
        m_cluQfitsig = new std::vector<float>;
        m_cluSfitrefit = new std::vector<uint8_t>;
        
        m_cluTree = new TTree("csc_cluster","list of any cluster");
        m_cluTree->Branch("run", &m_run, "run/I");
        m_cluTree->Branch("evt", &m_evt, "evt/I");
        m_cluTree->Branch("lumiBlockNum", &m_lumiBlockNum, "lumiBlockNum/I");
        m_cluTree->Branch("cluToMuon",&m_cluToMuon);
        
        m_cluTree->Branch("x",&m_clux);
        m_cluTree->Branch("y",&m_cluy);
        m_cluTree->Branch("z",&m_cluz);
        m_cluTree->Branch("r",&m_clur);
        
        m_cluTree->Branch("measphi",&m_cluMeasphi);
        m_cluTree->Branch("wlay", &m_cluWlay);
        m_cluTree->Branch("sector", &m_cluSector);
        m_cluTree->Branch("pos", &m_cluPos );
        m_cluTree->Branch("error", &m_cluDpos);
        m_cluTree->Branch("sfit", &m_cluSfit);
        m_cluTree->Branch("tfit", &m_cluTfit);
        m_cluTree->Branch("time", &m_cluTime);
        m_cluTree->Branch("qsum", &m_cluQsum);
        m_cluTree->Branch("pstrip", &m_cluPstrip);
        m_cluTree->Branch("nstrip", &m_cluNstrip);
        m_cluTree->Branch("strip0", &m_cluStrip0);
        m_cluTree->Branch("qpeak",  &m_cluQpeak);
        m_cluTree->Branch("qleft",  &m_cluQleft);
        m_cluTree->Branch("qright", &m_cluQright);
        m_cluTree->Branch("dqpeak", &m_cluDqpeak);
        m_cluTree->Branch("dqlef", &m_cluDqleft);
        m_cluTree->Branch("dqright", &m_cluDqright);
        m_cluTree->Branch("posrefit", &m_cluPosrefit);
        m_cluTree->Branch("errrefit", &m_cluDposrefit);
        m_cluTree->Branch("qfitdiff", &m_cluQfitdiff);
        m_cluTree->Branch("qfitsig", &m_cluQfitsig);
        m_cluTree->Branch("srefit", &m_cluSfitrefit);
    }
    
    
    if( m_fillSegmentBranches ){
        m_segToMuon = new std::vector<short>;
        m_segSector = new std::vector<short>;
        m_segTime = new std::vector<float>;
        m_segx = new std::vector<float>;
        m_segy = new std::vector<float>;
        m_segz = new std::vector<float>;
        m_segr = new std::vector<float>;
        m_segdirx = new std::vector<float>;
        m_segdiry = new std::vector<float>;
        m_segdirz = new std::vector<float>;
        m_segPhi = new std::vector<float>;
        m_segEta = new std::vector<float>;
        
        m_segPstrips = new std::vector< std::vector<int> >;
        m_segCharges = new std::vector< std::vector<float> >;
        m_segTimes   = new std::vector< std::vector<float> >;
        
        m_SegPosesX  = new std::vector< std::vector<float> >;
        m_SegPosesY  = new std::vector< std::vector<float> >;
        m_SegPosesZ  = new std::vector< std::vector<float> >;
        m_SegDPoses  = new std::vector< std::vector<float> >;
        
        m_SegSfits  = new std::vector< std::vector<int> >;
        
        m_segTree = new TTree("csc_segment","list of any segment");
        
        m_segTree->Branch("run", &m_run, "run/I");
        m_segTree->Branch("evt", &m_evt, "evt/I");
        m_segTree->Branch("lumiBlockNum", &m_lumiBlockNum, "lumiBlockNum/I");
        
        m_segTree->Branch("bcid", &m_bcid, "bcid/I");
        m_segTree->Branch("L1RD0", &m_L1RD0, "RD0/B");
        m_segTree->Branch("L1Mu", &m_L1Mu, "L1Mu/B");
        m_segTree->Branch("L2Mu", &m_L2Mu, "L2Mu/I");
        m_segTree->Branch("EFMu", &m_EFMu, "EFMu/I");
        
        m_segTree->Branch("gposx", &m_segx);
        m_segTree->Branch("gposy", &m_segy);
        m_segTree->Branch("gposz", &m_segz);
        m_segTree->Branch("gdirx", &m_segdirx);
        m_segTree->Branch("gdiry", &m_segdiry);
        m_segTree->Branch("gdirz", &m_segdirz);
        m_segTree->Branch("r", &m_segr);
        m_segTree->Branch("phi", &m_segPhi);
        m_segTree->Branch("eta", &m_segEta);
        m_segTree->Branch("segToMuon", &m_segToMuon);
        
        m_segTree->Branch("sector", &m_segSector);
        m_segTree->Branch("time", &m_segTime);
        
        m_segTree->Branch("pstrips", &m_segPstrips);
        m_segTree->Branch("charges", &m_segCharges);
        m_segTree->Branch("times", &m_segTimes);
        
        m_segTree->Branch("seg_posesX", &m_SegPosesX);
        m_segTree->Branch("seg_poses", &m_SegPosesY);
        m_segTree->Branch("seg_posesY", &m_SegPosesZ);
        
        m_segTree->Branch("seg_dposes", &m_SegDPoses);
        m_segTree->Branch("seg_sfits", &m_SegSfits);
        
    }
    
    m_recoTree->Branch("run", &m_run, "run/I");
    m_recoTree->Branch("evt", &m_evt, "evt/I");
    m_recoTree->Branch("lumiBlockNum", &m_lumiBlockNum, "lumiBlockNum/I");
    
    m_recoTree->Branch("bcid", &m_bcid, "bcid/I");
    m_recoTree->Branch("L1RD0", &m_L1RD0, "L1RD0/B");
    m_recoTree->Branch("L1Mu", &m_L1Mu, "L1Mu/B");
    m_recoTree->Branch("L2Mu", &m_L2Mu, "L2Mu/I");
    m_recoTree->Branch("EFMu", &m_EFMu, "EFMu/I");
    
    m_recoTree->Branch("muAuthors", &m_muAuthors);
    m_recoTree->Branch("trkPx", &m_trkPx);
    m_recoTree->Branch("trk_me_p", &m_trk_me_p);
    m_recoTree->Branch("trk_me_eta", &m_trk_me_eta);
    m_recoTree->Branch("trk_me_phi", &m_trk_me_phi);
    m_recoTree->Branch("trk_id_p", &m_trk_id_p);
    m_recoTree->Branch("trk_id_eta", &m_trk_id_eta);
    m_recoTree->Branch("trk_id_phi", &m_trk_id_phi);
    
    m_recoTree->Branch("trkPy", &m_trkPy);
    m_recoTree->Branch("trkPz", &m_trkPz);
    m_recoTree->Branch("trkP", &m_trkP);
    m_recoTree->Branch("trkPt", &m_trkPt);
    m_recoTree->Branch("trkEta", &m_trkEta);
    
    m_recoTree->Branch("trkPVXQoverP", &m_trkPVXQoverP);
    m_recoTree->Branch("trkPVXd0", &m_trkPVXd0);
    m_recoTree->Branch("trkPVXZ0", &m_trkPVXZ0);
    m_recoTree->Branch("trkPVXTheta", &m_trkPVXTheta );
    m_recoTree->Branch("trkPVXPhi0", &m_trkPVXPhi0  );
    
    m_recoTree->Branch("trkChisq", &m_trkChisq);
    m_recoTree->Branch("trkDoF", &m_trkDoF);
    
    m_recoTree->Branch("nBLYHit", &m_nBLYHit);
    m_recoTree->Branch("nPixelHit", &m_nPixelHit);
    m_recoTree->Branch("nSCTHit", &m_nSCTHit);
    m_recoTree->Branch("nTRTHit", &m_nTRTHit);
    m_recoTree->Branch("nTHTHit", &m_nTHTHit);
    
    m_recoTree->Branch("nMDTHit", &m_nMDTHit);
    m_recoTree->Branch("nTGCPHit", &m_nTGCPHit);
    m_recoTree->Branch("nTGCEHit", &m_nTGCEHit);
    m_recoTree->Branch("nRPCPHit", &m_nRPCPHit);
    m_recoTree->Branch("nRPCEHit", &m_nRPCEHit);
    m_recoTree->Branch("nCSCPHit", &m_nCSCPHit);
    m_recoTree->Branch("nCSCEHit", &m_nCSCEHit);
    m_recoTree->Branch("nMuSeg", &m_nMuSeg);
    
    m_recoTree->Branch("hitToMuon", &m_hitToMuon);
    m_recoTree->Branch("hitToSeg", &m_hitToSeg);
    
    m_recoTree->Branch("hit_sector", &m_hitSector);
    m_recoTree->Branch("hit_wlay", &m_hitWlay);
    m_recoTree->Branch("hit_measphi", &m_hitMeasphi);
    m_recoTree->Branch("hit_pstrip", &m_hitIstrip);
    
    m_recoTree->Branch("hitRecoTime", &m_hitRecoTime);
    m_recoTree->Branch("hitbeforeT0CorrTime", &m_hitbeforeT0CorrTime);
    m_recoTree->Branch("hitbeforeBPCorrTime", &m_hitbeforeBPCorrTime);
    
    m_recoTree->Branch("hit_time", &m_hitTime);
    m_recoTree->Branch("hit_sfit", &m_hitSfit);
    m_recoTree->Branch("hit_tfit", &m_hitTfit);
    m_recoTree->Branch("hit_pos", &m_hitPos);
    m_recoTree->Branch("hit_PhiPosReFit", &m_hitPhiReFit);
    m_recoTree->Branch("hit_dpos", &m_hitDPos);
    
    if (m_readESD) {
        m_recoTree->Branch("hit_nstr", &m_hitNstrip );
        m_recoTree->Branch("hit_str0", &m_hitStrip0 );
        m_recoTree->Branch("hit_qsum", &m_hitCharge );
        
        m_recoTree->Branch("hit_qpeak", &m_hitQpeak);
        m_recoTree->Branch("hit_qleft", &m_hitQleft);
        m_recoTree->Branch("hit_qright", &m_hitQright);
        m_recoTree->Branch("hit_dqpeak", &m_hitDqpeak);
        m_recoTree->Branch("hit_dqleft", &m_hitDqleft);
        m_recoTree->Branch("hit_dqright", &m_hitDqright);
        m_recoTree->Branch("hit_phase", &m_hitPhase);
        
        m_recoTree->Branch("hit_posrefit", &m_hitPosrefit);
        m_recoTree->Branch("hit_phiposrefit", &m_hitPhiPosrefit);
        m_recoTree->Branch("hit_dposrefit", &m_hitDposrefit);
        m_recoTree->Branch("hit_qfitsig", &m_hitQfitsig);
        m_recoTree->Branch("hit_qfitdiff", &m_hitQfitdiff);
        m_recoTree->Branch("hit_srefit", &m_hitSfitrefit);
    }
    
    m_recoTree->Branch("segToMuon", &m_assocSegToMuon);
    m_recoTree->Branch("seg_time", &m_assocSegTime);
    m_recoTree->Branch("seg_chsq", &m_assocSegChisq);
    m_recoTree->Branch("seg_ndof", &m_assocSegNDoF);
    m_recoTree->Branch("seg_gposx", &m_assocSegGposx);
    m_recoTree->Branch("seg_gposy", &m_assocSegGposy);
    m_recoTree->Branch("seg_gposz", &m_assocSegGposz);
    m_recoTree->Branch("seg_gdirx", &m_assocSegGdirx);
    m_recoTree->Branch("seg_gdiry", &m_assocSegGdiry);
    m_recoTree->Branch("seg_gdirz", &m_assocSegGdirz);
    m_recoTree->Branch("seg_posTheta", &m_assocSegPosTheta);
    m_recoTree->Branch("seg_posEta", &m_assocSegPosEta);
    m_recoTree->Branch("seg_posPhi", &m_assocSegPosPhi);
    m_recoTree->Branch("seg_dirTheta", &m_assocSegDirTheta);
    m_recoTree->Branch("seg_dirEta", &m_assocSegDirEta);
    m_recoTree->Branch("seg_dirPhi", &m_assocSegDirPhi);
    m_recoTree->Branch("seg_etalocpos", &m_assocSegEtalocpos);
    m_recoTree->Branch("seg_philocpos", &m_assocSegPhilocpos);
    m_recoTree->Branch("seg_etalocdir", &m_assocSegEtalocdir);
    m_recoTree->Branch("seg_philocdir", &m_assocSegPhilocdir);
    m_recoTree->Branch("seg_dy", &m_assocSegdy);
    m_recoTree->Branch("seg_dz", &m_assocSegdz);
    m_recoTree->Branch("seg_day", &m_assocSegday);
    m_recoTree->Branch("seg_daz", &m_assocSegdaz);
    m_recoTree->Branch("seg_eyz", &m_assocSegeyz);
    m_recoTree->Branch("seg_eyay", &m_assocSegeyay);
    m_recoTree->Branch("seg_eyaz", &m_assocSegeyaz);
    m_recoTree->Branch("seg_ezay", &m_assocSegezay);
    m_recoTree->Branch("seg_ezaz", &m_assocSegezaz);
    m_recoTree->Branch("seg_eayaz", &m_assocSegeayaz);
    
    m_recoTree->Branch("seg_sector", &m_assocSegSector);
    m_recoTree->Branch("seg_posesX", &m_assocSegPosesX);
    m_recoTree->Branch("seg_poses", &m_assocSegPosesY);
    m_recoTree->Branch("seg_posesZ", &m_assocSegPosesZ);
    m_recoTree->Branch("seg_dposes", &m_assocSegDPoses);
    m_recoTree->Branch("seg_pstrips", &m_assocSegPstrips);
    m_recoTree->Branch("seg_charges", &m_assocSegCharges);
    m_recoTree->Branch("seg_times", &m_assocSegTimes);
    m_recoTree->Branch("seg_sfits", &m_assocSegSfits);
    return;
}
//************************************************
void CreateNtuple::clearBranches(void){
    m_run = 0;
    m_evt = 0;
    m_lumiBlockNum = -99;
    m_bcid = -999.;
    m_L1RD0 = false;
    m_L1Mu = false;
    m_L2Mu = 0;
    m_EFMu = 0;
    
    m_muAuthors->clear();
    m_trkP->clear();
    m_trkPt->clear();
    m_trkPx->clear();
    m_trk_me_p->clear();
    m_trk_me_eta->clear();
    m_trk_me_phi->clear();
    m_trk_id_p->clear();
    m_trk_id_eta->clear();
    m_trk_id_phi->clear();
    
    
    m_trkPy->clear();
    m_trkPz->clear();
    m_trkEta->clear();
    
    m_trkPVXQoverP->clear();
    m_trkPVXd0->clear();
    m_trkPVXZ0->clear();
    m_trkPVXTheta->clear();
    m_trkPVXPhi0->clear();
    
    m_trkChisq->clear();
    m_trkDoF->clear();
    
    m_nBLYHit->clear();
    m_nPixelHit->clear();
    m_nSCTHit->clear();
    m_nTRTHit->clear();
    m_nTHTHit->clear();
    
    m_nMDTHit->clear();
    m_nTGCPHit->clear();
    m_nTGCEHit->clear();
    m_nRPCPHit->clear();
    m_nRPCEHit->clear();
    m_nCSCPHit->clear();
    m_nCSCEHit->clear();
    m_nMuSeg->clear();
    
    m_hitToMuon->clear();
    m_hitSector->clear();
    m_hitWlay->clear();
    m_hitMeasphi->clear();
    m_hitIstrip->clear();
    
    m_hitRecoTime->clear();
    m_hitbeforeT0CorrTime->clear();
    m_hitbeforeBPCorrTime->clear();
    
    m_hitTime->clear();
    m_hitSfit->clear();
    m_hitTfit->clear();
    m_hitPos->clear();
    m_hitPhiReFit->clear();
    m_hitDPos->clear();
    
    if (m_readESD) {
        m_hitCharge->clear();
        m_hitNstrip->clear();
        m_hitStrip0->clear();
        
        m_hitQpeak->clear();
        m_hitQleft->clear();
        m_hitQright->clear();
        m_hitDqpeak->clear();
        m_hitDqleft->clear();
        m_hitDqright->clear();
        m_hitPhase->clear();
        
        m_hitPosrefit->clear();
        m_hitPhiPosrefit->clear();
        m_hitDposrefit->clear();
        m_hitQfitsig->clear();
        m_hitQfitdiff->clear();
        m_hitSfitrefit->clear();
    }
    
    m_assocSegToMuon->clear();
    m_assocSegTime->clear();
    m_assocSegChisq->clear();
    m_assocSegNDoF->clear();
    m_assocSegGposx->clear();
    m_assocSegGposy->clear();
    m_assocSegGposz->clear();
    m_assocSegGdirx->clear();
    m_assocSegGdiry->clear();
    m_assocSegGdirz->clear();
    m_assocSegPosTheta->clear();
    m_assocSegPosEta->clear();
    m_assocSegPosPhi->clear();
    m_assocSegDirTheta->clear();
    m_assocSegDirEta->clear();
    m_assocSegDirPhi->clear();
    m_assocSegEtalocpos->clear();
    m_assocSegPhilocpos->clear();
    m_assocSegEtalocdir->clear();
    m_assocSegPhilocdir->clear();
    m_assocSegdy->clear();
    m_assocSegdz->clear();
    m_assocSegday->clear();
    m_assocSegdaz->clear();
    m_assocSegeyz->clear();
    m_assocSegeyay->clear();
    m_assocSegeyaz->clear();
    m_assocSegezay->clear();
    m_assocSegezaz->clear();
    m_assocSegeayaz->clear();
    
    m_hitToSeg->clear();
    m_assocSegSector->clear();
    
    for( uint i = 0; i < m_assocSegPstrips->size(); i++ ) m_assocSegPstrips[i].clear();
    m_assocSegPstrips->clear();
    
    for( uint i = 0; i < m_assocSegCharges->size(); i++ ) m_assocSegCharges[i].clear();
    m_assocSegCharges->clear();
    
    for( uint i = 0; i < m_assocSegTimes->size(); i++ ) m_assocSegTimes[i].clear();
    m_assocSegTimes->clear();
    
    for( uint i = 0; i < m_assocSegSfits->size(); i++ ) m_assocSegSfits[i].clear();
    m_assocSegSfits->clear();
    
    for( uint i = 0; i < m_assocSegPosesX->size(); i++ ) m_assocSegPosesX[i].clear();
    m_assocSegPosesX->clear();
    
    for( uint i = 0; i < m_assocSegPosesY->size(); i++ ) m_assocSegPosesY[i].clear();
    m_assocSegPosesY->clear();
    
    for( uint i = 0; i < m_assocSegPosesZ->size(); i++ ) m_assocSegPosesZ[i].clear();
    m_assocSegPosesZ->clear();
    
    for( uint i = 0; i < m_assocSegDPoses->size(); i++ ) m_assocSegDPoses[i].clear();
    m_assocSegDPoses->clear();
    
    for( uint i = 0; i < m_assocSegPosesX->size(); i++ ) m_assocSegPosesX[i].clear();
    m_SegPosesX->clear();
    
    for( uint i = 0; i < m_SegPosesY->size(); i++ ) m_SegPosesY[i].clear();
    m_SegPosesY->clear();
    
    for( uint i = 0; i < m_SegPosesZ->size(); i++ ) m_SegPosesZ[i].clear();
    m_SegPosesZ->clear();
    
    for( uint i = 0; i < m_SegDPoses->size(); i++ ) m_SegDPoses[i].clear();
    m_SegDPoses->clear();
    
    for( uint i = 0; i < m_SegSfits->size(); i++ ) m_SegSfits[i].clear();
    m_SegSfits->clear();
    
    if( m_fillClusterBranches ){
        m_cluToMuon->clear();
        m_cluMeasphi->clear();
        m_cluWlay->clear();
        m_cluSector->clear();
        m_cluPos->clear();
        
        m_cluDpos->clear();
        m_cluSfit->clear();
        m_cluTfit->clear();
        m_cluTime->clear();
        m_cluQsum->clear();
        
        m_cluPstrip->clear();
        m_cluNstrip->clear();
        m_cluStrip0->clear();
        m_cluQpeak->clear();
        m_cluQleft->clear();
        
        m_cluQright->clear();
        m_cluDqpeak->clear();
        m_cluDqleft->clear();
        m_cluDqright->clear();
        m_cluPosrefit->clear();
        
        m_cluDposrefit->clear();
        m_cluQfitdiff->clear();
        m_cluQfitsig->clear();
        m_cluSfitrefit->clear();
        m_clux->clear();
        
        m_cluy->clear();
        m_cluz->clear();
        m_clur->clear();
    }
    
    if( m_fillSegmentBranches ){
        m_segToMuon->clear();
        m_segSector->clear();
        m_segTime->clear();
        m_segx->clear();
        m_segy->clear();
        m_segz->clear();
        m_segr->clear();
        
        m_segdirx->clear();
        m_segdiry->clear();
        m_segdirz->clear();
        m_segPhi->clear();
        m_segEta->clear();
        
        for( uint i = 0; i < m_segPstrips->size(); i++ ) m_segPstrips[i].clear();
        m_segPstrips->clear();
        
        for( uint i = 0; i < m_segCharges->size(); i++ ) m_segCharges[i].clear();
        m_segCharges->clear();
        
        for( uint i = 0; i < m_segTimes->size(); i++ ) m_segTimes[i].clear();
        m_segTimes->clear();
        
    }
    
    return;
}
//*********************************************
StatusCode CreateNtuple::fillClusterBranches(){
    
    const DataHandle<Muon::CscPrepDataContainer> cols;
    if( evtStore()->retrieve(cols, "CSC_Clusters").isFailure() ){
        ATH_MSG_FATAL ("No cluster CscPrepDataContainer found in StoreGate!");
        return StatusCode::FAILURE;
    }
    
    //Fill the Clusters for the muons ntuple
    for( Muon::CscPrepDataContainer::const_iterator icol = cols->begin(); icol != cols->end(); icol++ ){
        const Muon::CscPrepDataCollection& clus = **icol;
        
        for( Muon::CscPrepDataCollection::const_iterator iclu = clus.begin(); iclu != clus.end(); ++iclu ){
            
            Muon::CscPrepData& clu = **iclu;
            Identifier cid = clu.identify();
            bool measphi = m_phelper->measuresPhi(cid);
            m_cluMeasphi->push_back( measphi );
            
            Trk::ParamDefs ierr = Trk::loc1;
            m_cluDpos->push_back( Amg::error(clu.localCovariance(),ierr) );
            
            int wlay = m_phelper->wireLayer(cid);
            m_cluWlay->push_back( wlay );
            
            int sector = m_phelper->sector(cid);
            m_cluSector->push_back( sector );
            
            Trk::ParamDefs icor = Trk::loc1;
            m_cluPos->push_back( clu.localPosition()[icor]);
            
            int pstrip = m_phelper->strip(cid);
            m_cluPstrip->push_back(pstrip);
            
            m_cluSfit->push_back(clu.status());
            m_cluTfit->push_back(clu.timeStatus());
            m_cluTime->push_back(clu.time());
            m_cluQsum->push_back(clu.charge());
            m_clux->push_back(clu.globalPosition().x());
            m_cluy->push_back(clu.globalPosition().y());
            m_cluz->push_back(clu.globalPosition().z());
            
            
            
        }
    }
    
    
    
    return StatusCode::SUCCESS;
}
//*******************************
int CreateNtuple::getNumberOfMatchedHits(CscSegmentInfo& seg, vector<CscHitInfo>& hits){
    
    int cntMatched = 0;
    
    if( hits[0].sector != seg.sector ) return cntMatched;
    
    vector<CscHitInfo> theHits = seg.hits;
    
    for( int il = 1; il < 5; il++ ){
        for(int im = 0; im < 2; im++ ){
            
            int theIdxStrip = -10;
            
            for( uint j = 0; j < theHits.size(); j++ ){
                if( il == int(theHits[j].wlay) && im == int(theHits[j].measphi) ){
                    theIdxStrip = theHits[j].istrip;
                    break;
                }
            }
            
            int anIdxStrip = -10;
            for( uint j = 0; j < hits.size(); j++ ){
                if( il == int(hits[j].wlay) && im ==int(hits[j].measphi) ){
                    anIdxStrip = hits[j].istrip;
                    break;
                }
            }
            
            if( theIdxStrip >= 0 && anIdxStrip == theIdxStrip ) cntMatched++;
            
        }
    }
    
    ATH_MSG_VERBOSE ( "nMatched is " << cntMatched );
    
    return cntMatched;
    
}
//*************************************************
StatusCode CreateNtuple::getEventInfo(){
    
    const EventInfo* eventInfo; // EventInfo
    if( evtStore()->retrieve(eventInfo).isFailure() )return StatusCode::FAILURE;
    
    EventID* eventID = eventInfo->event_ID(); // Get EventInfo
    //TriggerInfo* triggerInfo = eventInfo->trigger_info(); // Get Trigger Info
    
    m_run = eventID->run_number();
    m_evt = eventID->event_number();
    
    m_L1RD0 = false;
    m_L1Mu = false;
    
    m_bcid = eventID->bunch_crossing_id();
    
    if( !m_readMC ){
        m_lumiBlockNum =eventID->lumi_block(); // even in MC it has reasonable number...
        //m_bcid = eventID->bunch_crossing_id(); //Bunch Crossing IDentification
        /*
         //Retrieve Tigger Info
         //L1 - RD0
         const unsigned word_size_in_bits = sizeof(uint32_t)*8;
         uint bit = 240;
         uint word = bit/word_size_in_bits;
         uint shift = bit%word_size_in_bits;
         std::vector<uint32_t> bitarray = triggerInfo->level1TriggerInfo();
         uint offset = 0;
         
         if( 24== bitarray.size() )offset=16;
         else {
         ATH_MSG_ERROR ( "Severe Trigger Error - Bit Array size WRONG!!!!!" );
         return -1;
         }
         
         if ((word+offset)<bitarray.size()) m_L1RD0 = (bitarray[word+offset] & 0x1 << shift);
         else ATH_MSG_WARNING ( " WARNING : outside bitarray size " );
         
         //L1 Muons
         offset = 0;
         
         if(24==bitarray.size()){
         offset=16;
         for(bit = 32; bit<38; bit++){
         shift = bit%word_size_in_bits;
         word = bit/word_size_in_bits;
         if ((word+offset)<bitarray.size()) m_L1Mu = (bitarray[word+offset] & 0x1 << shift);
         if(m_L1Mu)break;
         }
         }
         
         
         //m_L2Mu = triggerInfo->level2TriggerInfo(); // Level 2 trigger
         //m_EFMu = triggerInfo->eventFilterInfo(); // Level 1 trigger*/
    }
    else{
        m_lumiBlockNum = -99; // but to mark this sample is from MC, I put negative number here...
        //m_bcid = -999;
        //m_L1Mu = 0;
        //m_L2Mu = 0;
        //m_EFMu = 0;
    }
    
    return StatusCode::SUCCESS;
}

//*******************************************


