//====================================================================
//  LeptonDMesonAlgs_entries.cxx
//--------------------------------------------------------------------
//
//  Description: Implementation of <Package>_load routine.
//               This routine is needed for forcing the linker
//               to load all the components of the library. 
//
//====================================================================

#include "../CreateNtuple.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( CreateNtuple )

DECLARE_FACTORY_ENTRIES(CreateNtuple) {
  DECLARE_ALGORITHM( CreateNtuple )
}
