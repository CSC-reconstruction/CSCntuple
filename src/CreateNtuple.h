#ifndef CREATENTUPLE_H
#define CREATENTUPLE_H

#include <string>
#include <vector>
#include <list>
#include <map>

#include "GaudiKernel/ServiceHandle.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" 
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/Muon.h"

#include "xAODMuon/MuonSegmentAuxContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "TrkSegment/SegmentCollection.h"
#include "TrkSegment/Segment.h"
#include "MuonSegment/MuonSegment.h"
#include "AthLinks/ElementLink.h"

#include "EventPrimitives/EventPrimitivesHelpers.h"

#include "MuonIdHelpers/MuonIdHelperTool.h"
#include "MuonCalibITools/IIdToFixedIdTool.h"
#include "MuonRecHelperTools/MuonEDMHelperTool.h"

#include "CscClusterization/ICscClusterFitter.h"
#include "CscClusterization/ICscClusterUtilTool.h"

#include "TrkTrackSummary/TrackSummary.h"

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"

//#include "xAODMuonCnv/MuonSegmentConverterTool.h"


//#include "xAODTruth/TruthVertexContainer.h"
//#include "xAODTruth/TruthVertexAuxContainer.h"
//#include "xAODEventInfo/EventInfo.h"
//#include "xAODBase/IParticleHelpers.h"
//#include "xAODCore/ShallowCopy.h"

using namespace std;

class TFile;
class TTree;

namespace MuonGM {
  class MuonDetectorManager;
}
/*namespace Analysis {
  class Muon;
}*/
namespace Muon {
  class MuonSegment;
  class MuonIdHelperTool;
  class MuonEDMHelperTool;
}

class CscIdHelper;

//
class CreateNtuple: public AthAlgorithm {

  /************************************** public *********************************************/
  
 public:
  class CscHitInfo {
    
  public:
    int sector; unsigned int wlay; bool measphi; int istrip; int status;
    CscHitInfo(int s, unsigned int w, bool m, int i, int st) :
      sector(s), wlay(w), measphi(m), istrip(i), status(st) {}
  };
  
  class CscSegmentInfo {
  public:
    unsigned int muonIdx; int sector; vector<CscHitInfo> hits;
  CscSegmentInfo(unsigned int muIdx, int s, vector<CscHitInfo> hs ) :
    muonIdx(muIdx), sector(s), hits(hs) {}
    
  };
  
  CreateNtuple(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

  StatusCode getEventInfo();


  StatusCode fillClusterBranches();

  void initializeBranches(void);
  void clearBranches(void);

  int getNumberOfMatchedHits(CscSegmentInfo& assocSegments, vector<CscHitInfo>& hits);
  
  /************************************** private *********************************************/
 private:

  // Pointer to muon geometry manager.
  const MuonGM::MuonDetectorManager* m_pmuon_detmgr;
  
  // Geometry helper.
  const CscIdHelper* m_phelper;

  
  TFile* OutputFile; 
  TTree* m_recoTree;
  TTree* m_cluTree;
  TTree* m_segTree;
  
  bool m_readMC;
  bool m_readESD;
  bool m_dumpNtuple;
  bool m_fillClusterBranches;
  bool m_fillSegmentBranches;
  float m_muonMomentumCut;
  
  std::string m_containerName;
  std::string m_muonCollName;
  std::string m_userFName;
  std::string   m_muonSegmentLocation;
  std::string m_muonSegmenteCollectionName;

  ToolHandle<MuonCalib::IIdToFixedIdTool> m_idToFixedIdTool;
  ToolHandle<Muon::MuonEDMHelperTool> m_helperTool;
  ToolHandle<Muon::MuonIdHelperTool>  m_idHelperTool;
  ToolHandle<ICscClusterFitter> m_cluster_fitter;
  ToolHandle<ICscClusterUtilTool> m_clusterTool;
  ToolHandle<ICscStripFitter>   m_stripFitter;

  /*ToolHandle<ICscClusterUtilTool> m_clusterTool;
  ToolHandle<ICscSegmentUtilTool> m_segmentTool;
  ToolHandle<ICscClusterFitter> m_cluster_fitter;
  ToolHandle<MuonCalib::IIdToFixedIdTool> m_idToFixedIdTool;*/
 
  //**************************
  //***   ntuple content   ***
  //**************************
  
  int m_run;
  int m_evt;
  int m_lumiBlockNum;
  int m_bcid;
  bool m_L1RD0;
  bool m_L1Mu; 
  int m_L2Mu;
  int m_EFMu;

  int n_allMuons;
  int n_CombMuons;
  int n_TsosCollMuons;
  int n_TsosTypeMuons;
  int n_RIO_OnTrackMuons;
  int n_eta;
  int n_idHelperToolMuons1,n_idHelperToolMuons2,n_idHelperToolMuons3,n_idHelperToolMuons4;
  int n_trackParsMuons;
  int n_assocSegments;
  int n_Segments;

  
  std::vector<uint8_t> *m_nBLYHit;   
  std::vector<uint8_t> *m_nPixelHit;
  std::vector<uint8_t> *m_nSCTHit;
  std::vector<uint8_t> *m_nTRTHit;
  std::vector<uint8_t> *m_nTHTHit;

  std::vector<uint8_t> *m_nMDTHit ;
  std::vector<uint8_t> *m_nTGCPHit;
  std::vector<uint8_t> *m_nTGCEHit;
  std::vector<uint8_t> *m_nRPCPHit;
  std::vector<uint8_t> *m_nRPCEHit;
  std::vector<uint8_t> *m_nCSCPHit;
  std::vector<uint8_t> *m_nCSCEHit;
  std::vector<uint8_t> *m_nMuSeg;

  std::vector<uint16_t> * m_muAuthors;
  std::vector<float> * m_trkPx;
  std::vector<float> * m_trk_me_p;
  std::vector<float> * m_trk_me_eta;
  std::vector<float> * m_trk_me_phi;
  std::vector<float> * m_trk_id_p;
  std::vector<float> * m_trk_id_eta;
  std::vector<float> * m_trk_id_phi;

  std::vector<float> * m_trkPy;
  std::vector<float> * m_trkPz;
  std::vector<float> * m_trkPt;
  std::vector<float> * m_trkP;
  std::vector<float> * m_trkEta;
  std::vector<float> * m_trkPhi;
  std::vector<float> * m_trkM;
  std::vector<float> * m_trkE;

  std::vector<float> *m_trkPVXQoverP;
  std::vector<float> *m_trkPVXd0;
  std::vector<float> *m_trkPVXZ0;
  std::vector<float> *m_trkPVXTheta;
  std::vector<float> *m_trkPVXPhi0;
  
  std::vector<float> *m_trkChisq;
  std::vector<uint8_t> *m_trkDoF;

  std::vector<uint8_t> *m_hitToMuon;
  std::vector<short> *m_hitSector;
  std::vector<uint8_t> *m_hitWlay;
  std::vector<bool> *m_hitMeasphi;
  std::vector<uint8_t> *m_hitIstrip;

  std::vector<float> *m_hitCharge; 
  std::vector<float> *m_hitTime; 
  std::vector<float> *m_hitRecoTime;
  std::vector<float> *m_hitbeforeT0CorrTime;
  std::vector<float> *m_hitbeforeBPCorrTime;

  std::vector<uint8_t> *m_hitSfit; 
  std::vector<uint8_t> *m_hitTfit; 
  std::vector<uint8_t> *m_hitNstrip; 
  std::vector<uint8_t> *m_hitStrip0; 
  std::vector<float> *m_hitPos; 
  std::vector<float> *m_hitPhiReFit;
  std::vector<float> *m_hitDPos; 

  std::vector<float> *m_hitQpeak; 
  std::vector<float> *m_hitQleft;
  std::vector<float> *m_hitQright;
  std::vector<float> *m_hitDqpeak;
  std::vector<float> *m_hitDqleft;
  std::vector<float> *m_hitDqright;
  std::vector<bool> *m_hitPhase;

  std::vector<float> *m_hitPosrefit;
  std::vector<float> *m_hitPhiPosrefit;
  std::vector<float> *m_hitDposrefit;
  std::vector<float> *m_hitQfitsig;
  std::vector<float> *m_hitQfitdiff;
  std::vector<uint8_t> *m_hitSfitrefit;
  
  std::vector<uint8_t> *m_assocSegToMuon;
  std::vector<float> *m_assocSegTime;
  std::vector<float> *m_assocSegChisq;
  std::vector<float> *m_assocSegNDoF;
  std::vector<float> *m_assocSegGposx;
  std::vector<float> *m_assocSegGposy;
  std::vector<float> *m_assocSegGposz;
  std::vector<float> *m_assocSegGdirx;
  std::vector<float> *m_assocSegGdiry;
  std::vector<float> *m_assocSegGdirz;
  std::vector<float> *m_assocSegPosTheta;
  std::vector<float> *m_assocSegPosEta;
  std::vector<float> *m_assocSegPosPhi;
  std::vector<float> *m_assocSegDirTheta;
  std::vector<float> *m_assocSegDirEta;
  std::vector<float> *m_assocSegDirPhi;
  std::vector<float> *m_assocSegEtalocpos;
  std::vector<float> *m_assocSegPhilocpos;
  std::vector<float> *m_assocSegEtalocdir;
  std::vector<float> *m_assocSegPhilocdir;
  std::vector<float> *m_assocSegdy;
  std::vector<float> *m_assocSegdz;
  std::vector<float> *m_assocSegday;
  std::vector<float> *m_assocSegdaz;
  std::vector<float> *m_assocSegeyz;
  std::vector<float> *m_assocSegeyay;
  std::vector<float> *m_assocSegeyaz;
  std::vector<float> *m_assocSegezay;
  std::vector<float> *m_assocSegezaz;
  std::vector<float> *m_assocSegeayaz;

  std::vector<uint8_t> *m_hitToSeg;

  std::vector<short>   *m_assocSegSector;
  std::vector< std::vector<int>> *m_assocSegPstrips;
  std::vector< std::vector<float>> *m_assocSegCharges ;
  std::vector< std::vector<float>> *m_assocSegTimes;
  std::vector< std::vector<float>> *m_assocSegPosesX;
  std::vector< std::vector<float>> *m_assocSegPosesY;
  std::vector< std::vector<float>> *m_assocSegPosesZ;
  std::vector< std::vector<float>> *m_assocSegDPoses;
  std::vector< std::vector<int>> *m_assocSegSfits;
  //
  std::vector<short> *m_cluToMuon;
  std::vector<float> *m_clux;
  std::vector<float> *m_cluy;
  std::vector<float> *m_cluz;
  std::vector<float> *m_clur;
  std::vector<bool> *m_cluMeasphi;
  std::vector<uint8_t> *m_cluWlay;
  std::vector<short> *m_cluSector;
  std::vector<float> *m_cluPos;
  std::vector<float> *m_cluDpos;
  std::vector<uint8_t> *m_cluSfit;
  std::vector<uint8_t> *m_cluTfit;
  std::vector<float> *m_cluTime;
  std::vector<float> *m_cluQsum;
  std::vector<uint8_t> *m_cluPstrip;
  std::vector<uint8_t> *m_cluNstrip;
  std::vector<uint8_t> *m_cluStrip0;
  std::vector<float> *m_cluQpeak;
  std::vector<float> *m_cluQleft;
  std::vector<float> *m_cluQright;
  std::vector<float> *m_cluDqpeak;
  std::vector<float> *m_cluDqleft;
  std::vector<float> *m_cluDqright;
  std::vector<float> *m_cluPosrefit;
  std::vector<float> *m_cluDposrefit;
  std::vector<float> *m_cluQfitdiff;
  std::vector<float> *m_cluQfitsig;
  std::vector<uint8_t> *m_cluSfitrefit;
  //
  std::vector<short> *m_segToMuon;
  std::vector<short> *m_segSector;
  std::vector<float> *m_segTime;
  std::vector<float> *m_segx;
  std::vector<float> *m_segy;
  std::vector<float> *m_segz;
  std::vector<float> *m_segr;
  std::vector<float> *m_segdirx;
  std::vector<float> *m_segdiry;
  std::vector<float> *m_segdirz;
  std::vector<float> *m_segPhi;
  std::vector<float> *m_segEta;

  std::vector<int> *m_segPHits;

  std::vector< std::vector<int>> *m_segPstrips;
  std::vector< std::vector<float>> *m_segCharges;
  std::vector< std::vector<float>> *m_segTimes;
  std::vector< std::vector<float>> *m_SegPosesX;
  std::vector< std::vector<float>> *m_SegPosesY;
  std::vector< std::vector<float>> *m_SegPosesZ;
  std::vector< std::vector<float>> *m_SegDPoses;

  std::vector< std::vector<int>> *m_SegSfits;
  
  int m_evtCnt;
  string m_segmentKey;
  
};

#endif
