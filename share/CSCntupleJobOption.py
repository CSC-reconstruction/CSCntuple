# ------------------------------------------------------------
## This Athena job consists of algorithms that loop over events;
## here, the (default) top sequence is used:
print 'begin CSCntupleJobOption'

from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
job = AlgSequence()
job += AthSequencer("ModSequence1")
    
from RecExConfig.InputFilePeeker import inputFileSummary
isMC = False if inputFileSummary['evt_type'][0] == 'IS_DATA'  else True
isESD = True if "StreamESD" in inputFileSummary['stream_names'] else False

print "Using stream isMC, isESD: ", isMC, isESD

from AthenaCommon.GlobalFlags import GlobalFlags
GlobalFlags.DetGeo='atlas'

if not isMC:
    GlobalFlags.DataSource='data'
#    GlobalFlags.DataSource.set_data() ## Needed for REAL DATA, not MC

#-------------------------------------------------------------
# User analysis steering algorithm
#-------------------------------------------------------------
from CSCntuple.CSCntupleConf import CreateNtuple
job.ModSequence1 += CreateNtuple('CreateNtuple') #here the new ntuple is created

# Runtime settings 
job.ModSequence1.CreateNtuple.MuonCollectionName = MuonCollectionName 
#job.ModSequence1.CreateNtuple.MissingEtName      = "MET_Final" 
job.ModSequence1.CreateNtuple.outputNTupleName   = NtupleFileName
job.ModSequence1.CreateNtuple.readMC             = isMC
#job.ModSequence1.CreateNtuple.SkipEvents         = SkipEvents
job.ModSequence1.CreateNtuple.fillClusterBranches= FillClusterBranches
#job.ModSequence1.CreateNtuple.MuonCollectionName = MuonCollectionName
job.ModSequence1.CreateNtuple.readESD = isESD
job.ModSequence1.CreateNtuple.muonMomentumCut = MuonMomentumCut

# This is still picked up in new schema of retrieval method...
from AthenaCommon.CfgGetter import getPublicTool

import CscSegmentMakers.CscSegmentMakersConf
from CscSegmentMakers.CscSegmentMakersConf import CscSegmentUtilTool
#job += getPublicTool("CscSegmentUtilTool")
#job += getPublicTool("CscSegmentUtilTool",checkType=False)


# This is still picked up in new schema of retrieval method...
##from CscClusterization.CscThresholdClusterBuilder import theCscThresholdClusterBuilder
##job += theCscThresholdClusterBuilder   #to debug the phi off

##from CscClusterization.QratCscClusterFitter import QratCscClusterFitter
##ToolSvc.QratCscClusterFitter.max_width    = [100,100,100,100]
#ToolSvc.QratCscClusterFitter.OutputLevel = VERBOSE
#ToolSvc.QratCscClusterFitter.qrat_maxdiff = 1000.
##ToolSvc.QratCscClusterFitter.qrat_maxsig  = 6.
##ToolSvc.QratCscClusterFitter.position_option_eta = "ATANH"
#ToolSvc.QratCscClusterFitter.position_option_phi = "NONE"
#ToolSvc.QratCscClusterFitter.error_option_eta = "CHARGE"
#ToolSvc.QratCscClusterFitter.error_option_phi = "NONE"
#ToolSvc.QratCscClusterFitter.error_tantheta = 0.57
#ToolSvc.QratCscClusterFitter.xtan_css_eta_offset = 0.0015
#ToolSvc.QratCscClusterFitter.xtan_css_eta_slope = 0.000137
#ToolSvc.QratCscClusterFitter.xtan_csl_eta_offset = -.0045
#ToolSvc.QratCscClusterFitter.xtan_csl_eta_slope = 0.000131
#ToolSvc.QratCscClusterFitter.qratmin_css_eta = 0.0940459
                                
if Debug:
    job.ModSequence1.CreateNtuple.OutputLevel = DEBUG

if Verbose:
    job.ModSequence1.CreateNtuple.OutputLevel = VERBOSE

print 'end CSCntupleJobOption'

